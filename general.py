# -*- coding: utf-8 -*-
"""
Created on Thu Nov 16 23:57:50 2017

@author: Edwards
"""

import numpy as np
import scipy as sp
#import scipy.stats

from _constants import c, pi
#from numpy import ma


def lam2f(lam):
    """
    convert wavelength to frequency

    Parameters
    ----------
    lam : float
        wavelength (m)

    Returns
    -------
    float
        frequency (Hz)

    """
    return c/lam


def f2lam(f):
    """
    convert frequency to wavelength

    Parameters
    ----------
    f : float
        frequency (Hz)

    Returns
    -------
    float
        wavelength (m)

    """
    return c/f


def fci(x, val):
    """
    Return index of closest value in given vector

    Parameters
    ----------
    x : array
        input vector
    val : int or float
        value to find in input vector

    Returns
    -------
    int
        index of x that most closely matches val

    """
    return np.argmin(np.abs(x-val))


def samps2time(n_samps, Fs):
    """
    Convert number of samples to time given sample rate

    Parameters
    ----------
    n_samps : int
        number of samples
    Fs : float
        sample rate of discrete time data

    Returns
    -------
    float
        time corresponding to specified number of samples in seconds

    """
    return n_samps/Fs


def time2samps(t, Fs):
    """
    Convert time to number of samples given sample rate

    Parameters
    ----------
    t : float
        time in seconds
    Fs : float
        sample rate of discrete time data

    Returns
    -------
    float
        number of samples corresponding to specified time

    """
    return t*Fs


def power2dB(power, base='dB'):
    """
    Convert power ratio to decibels

    Parameters
    ----------
    power : float
        power ratio
    base : {'db' (default), 'dBm'}
        indicates power ratio is relative to 1W (dB) or 1 mW (dBm)

    Returns
    -------
    float
        power ratio in decibels

    """

    # if input is single number and less than or equal to zero, return 0
    if isinstance(power, int) or isinstance(power, float):
        if power <= 0:
            return -np.inf
        else:
            out = 10*np.log10(power)

    # if an array, set output for entries lte 0 to -inf
    else:
        out = np.array(power)
        out[np.isnan(out)] = -np.inf
        out[out <= 0] = -np.inf
        out[out > 0] = 10*np.log10(out[out > 0])

    if base == 'dBm':
        out = out + 30

    # return value
    return out


def mag2dB(mag, base='dB'):
    """
    Convert magnitude ratio to decibels

    Parameters
    ----------
    mag : float
        magnitude

    Returns
    -------
    float
        magnitude ratio in decibels

    """
    # if input is single number and less than or equal to zero, return 0
    if isinstance(mag, int) or isinstance(mag, float):
        if mag <= 0:
            return -np.inf
        else:
            out = 20*np.log10(mag)

    # if an array, set output for entries lte 0 to -inf
    else:
        out = np.array(mag)
        out[np.isnan(out)] = -np.inf
        out[out <= 0] = -np.inf
        out[out > 0] = 20*np.log10(out[out > 0])

    if base == 'dBm':
        out = out + 30

    # return value
    return out


def dB2power(dB):
    """
    Convert decibels to power ratio

    Parameters
    ----------
    dB : float
        decibels

    Returns
    -------
    float
        power ratio

    """
    return 10**(dB/10)


def dB2mag(dB):
    """
    Convert decibels to magnitude ratio

    Parameters
    ----------
    dB : float
        decibels

    Returns
    -------
    float
        magnitude ratio

    """
    return 10**(dB/20)


def rad2deg(rad):
    """
    Convert decibels to power ratio

    Parameters
    ----------
    rad : float
        radians

    Returns
    -------
    float
        degrees

    """
    return rad*180/pi


def deg2rad(deg):
    """
    Convert degrees to radians

    Parameters
    ----------
    deg : float
        degrees

    Returns
    -------
    float
        radians

    """
    return deg*pi/180


def fd2Rdot(fd, f, way=2):
    """
    convert Dopper shift to range rate

    reference: Basic Radar Analysis (Budge), pg 11, eq 1.25

    Parameters
    ----------
    fd : float
        Doppler shift (Hz)
    f : float
        transmit frequency (Hz)
    way : int (2 default)
        number of legs in path, ie. 1 or 2 way delay

    Returns
    -------
    float
        range rate (m/s)

    """
    return -fd*c/(way*f)


def Rdot2fd(Rdot, f, way=2):
    """
    convert range rate to Doppler shift

    reference: Basic Radar Analysis (Budge), pg 11, eq 1.25

    Parameters
    ----------
    Rdot : float
        Doppler shift (m/s)
    f : float
        transmit frequency (Hz)
    way : int (2 default)
        number of legs in path, ie. 1 or 2 way delay

    Returns
    -------
    float
        Doppler shift (Hz)

    """
    return -f*way*Rdot/c


def R2tau(R, way=2):
    """
    convert range to time delay

    reference: Basic Radar Analysis (Budge), pg 4, eq 1.4

    Parameters
    ----------
    R : float
        range (m)
    way : int (2 default)
        number of legs in path, ie. 1 or 2 way delay

    Returns
    -------
    float
        tau delay (s)

    """
    return way*R/c


def tau2R(tau, way=2):
    """
    convert from time delay to range

    reference: Basic Radar Analysis (Budge), pg 5, eq 1.6

    Parameters
    ----------
    tau : float
        time delay (s)
    way : int (2 default)
        number of legs in path, ie. 1 or 2 way delay

    Returns
    -------
    float
        R range (m)

    """
    return c*tau/way


def freqBand(f, des='IEEE'):
    '''
    returns name of frequency band in specified refeerence designation system

    based on Wikipedia's Radio Spectrum page:
    (https://en.wikipedia.org/wiki/Radio_spectrum)
    ICOD: 20180109
    '''
    if des.lower() == 'ieee':
        if (f >= 0.003e9) & (f < 0.03e9):
            return 'HF'
        elif (f >= 0.03e9) & (f < 0.3e9):
            return 'VHF'
        elif (f >= 0.3e9) & (f < 1e9):
            return 'UHF'
        elif (f >= 1e9) & (f < 2e9):
            return 'L'
        elif (f >= 2e9) & (f < 4e9):
            return 'S'
        elif (f >= 4e9) & (f < 8e9):
            return 'C'
        elif (f >= 8e9) & (f < 12e9):
            return 'X'
        elif (f >= 12e9) & (f < 18e9):
            return 'Ku'
        elif (f >= 18e9) & (f < 27e9):
            return 'K'
        elif (f >= 27e9) & (f < 40e9):
            return 'Ka'
        elif (f >= 40e9) & (f < 75e9):
            return 'V'
        elif (f >= 75e9) & (f < 110e9):
            return 'W'
        elif (f >= 110e9) & (f < 300e9):
            return 'mm'
        else:
            return 'Unknown'
    elif des.lower() in ('eu', 'nato', 'us ecm'):
        if (f >= 0) & (f < 0.25e9):
            return 'A'
        elif (f >= 0.25e9) & (f < 0.5e9):
            return 'B'
        elif (f >= 0.5e9) & (f < 1e9):
            return 'C'
        elif (f >= 1e9) & (f < 2e9):
            return 'D'
        elif (f >= 2e9) & (f < 3e9):
            return 'E'
        elif (f >= 3e9) & (f < 4e9):
            return 'F'
        elif (f >= 4e9) & (f < 6e9):
            return 'G'
        elif (f >= 6e9) & (f < 8e9):
            return 'H'
        elif (f >= 8e9) & (f < 10e9):
            return 'I'
        elif (f >= 10e9) & (f < 20e9):
            return 'J'
        elif (f >= 20e9) & (f < 40e9):
            return 'K'
        elif (f >= 40e9) & (f < 60e9):
            return 'L'
        elif (f >= 60e9) & (f < 100e9):
            return 'M'
        else:
            return 'Unknown'


def des2func(des='TPY'):
    '''
    returns the explanation of JETDS letter designations

    based on Skolnik's Radar Handbood
    '''
    # Installation (first letter)
    if des[0].lower() == 'a':
        install = 'Piloted aircraft'
    elif des[0].lower() == 'b':
        install = 'Underwater mobile, submarine'
    elif des[0].lower() == 'd':
        install = 'Pilotless aircraft'
    elif des[0].lower() == 'f':
        install = 'Fixed ground'
    elif des[0].lower() == 'g':
        install = 'General ground use'
    elif des[0].lower() == 'k':
        install = 'Amphibious'
    elif des[0].lower() == 'm':
        install = 'Mobile (ground)'
    elif des[0].lower() == 'p':
        install = 'Portable'
    elif des[0].lower() == 's':
        install = 'Water (ship)'
    elif des[0].lower() == 't':
        install = 'Transportable (ground)'
    elif des[0].lower() == 'u':
        install = 'General utility'
    elif des[0].lower() == 'v':
        install = 'Vehicular (ground)'
    elif des[0].lower() == 'w':
        install = 'Water surface and underwater combined'
    elif des[0].lower() == 'z':
        install = 'Piloted-pilotless airborne vehicles combined'
    else:
        install = 'Unknown installation'

    # Type of Equipment (second letter)
    if des[1].lower() == 'l':
        equip = 'Countermeasures'
    elif des[1].lower() == 'p':
        equip = 'Radar'
    elif des[1].lower() == 's':
        equip = 'Special or combination'
    elif des[1].lower() == 'w':
        equip = 'Armament (peculiar to armament not otheriwse covered)'
    else:
        equip = 'Unknown equipment'

    # Installation (first letter)
    if des[2].lower() == 'b':
        purpose = 'Bombing'
    elif des[2].lower() == 'd':
        purpose = 'Direction finder, reconnaissance and surveillance'
    elif des[2].lower() == 'g':
        purpose = 'Fire control'
    elif des[2].lower() == 'n':
        purpose = 'Navigation'
    elif des[2].lower() == 'q':
        purpose = 'Special or combination'
    elif des[2].lower() == 'r':
        purpose = 'Receiving'
    elif des[2].lower() == 's':
        purpose = 'Detecting / range and bearing, search'
    elif des[2].lower() == 't':
        purpose = 'Transmitting'
    elif des[2].lower() == 'w':
        purpose = 'Automatic flight or remote control'
    elif des[2].lower() == 'x':
        purpose = 'Identification and recognition'
    elif des[2].lower() == 'y':
        purpose = 'Surveillance & control (both fire control & air control)'
    else:
        purpose = 'Unknown purpose'

    return install + ', ' + equip + ', ' + purpose


def azel2uv(az, el):
    """
    Convert azimuth and elevation angles to sine space u and v

    Parameters
    ----------
    az : float
        azimuth in radians
    el : float
        elevation in radians

    Returns
    -------
    u, v = list
        list of u and v vectors

    """
    return np.sin(az)*np.cos(el), np.sin(el)


def uv2azel(u, v):
    """
    Convert u and v to azimuth and elevation

    Parameters
    ----------
    u : float
        azimuth in radians
    v : float
        elevation in radians

    Returns
    -------
    az, el = list
        list of azimuth and elevation vectors in radians

    """
    uv2 = u**2+v**2
    return np.arctan2(u, np.sqrt(1-np.ma.masked_array(uv2, uv2 > 1))), \
        np.arcsin(v)


def upsamp(x, F):
    '''
    Upsample a vector or array by insterting 0's

    Parameters
    ----------
    x : ndarray
        input
    F : int
        upsample factor

    Return
    ------
    ndarray
        upsampled array
    '''

    N = x.shape
    if np.array(F).size == 1:
        y = np.zeros((int(F*(np.prod(N)-1)+1), 1))
        y[::int(F), 0] = x[:, 0]
    elif np.array(F).size == 2:
        y = np.zeros((int(F[0]*(N[0]-1)+1), int(F[1]*(N[1]-1)+1)))

        for row in range(0, int(F[0]*(N[0]-1)+1), int(F[0])):
            y[row, ::int(F[1])] = x[int(row/F[0]), :]

    return y


def nextpow2(n):
    '''
    Returns the next highest power of 2

    Parameter
    ---------
    n : int
        number to find next highet power of 2 of
    '''

    return 2**np.ceil(np.log2(n)).astype(int)
