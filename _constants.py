# -*- coding: utf-8 -*-
"""
Created on Fri Dec 29 17:10:02 2017

@author: Edwards
"""

from numpy import pi, nan

# Speed of light
c = 3e8
c_exact = 299792458

# Boltzman's Constant
k = 1.38e-23

# Noise Reference Temperature
T0 = 290

# Earth Radius
Re = 6371000