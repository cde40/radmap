# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 13:25:09 2017

@author: Edwards
"""

from general import fci, power2dB, dB2power, azel2uv, uv2azel, upsamp, nextpow2
from _constants import c, nan
import matplotlib.pyplot as plt
import numpy as np
from numpy import pi, exp, sin, cos, sqrt, max, min, nonzero
from numpy.fft import fftshift, fftfreq, fft2, fft
import scipy as sp
import scipy.signal


class Antenna():
    """
    Basic antenna class when little information is known

    Parameters
    ----------
    f : float
        frequency in Hz
    BW : float or list of floats, optional
        half power beam widthd(s)
    dim : float or list of floats, optional
        antenna dimensions in m
    rho : float (0.6 default), optional
        antenna efficiency
    az0, el0 : float (0 deg, 0 deg default), optional
        az / el pointing direction
    h : float (0 m default), optional
        antenna (phase center) height in m
    SL : negative float (-20 dB default), optional
        nominal sidelobe level

    Methods
    -------
    steer()
        set steer angle of antenna
    mba()
        main beam approximation (Gaussian model)
    get_BW()
        return half power beam width
    get_G()
        return directivity, G

    Returns
    -------
    Antenna object

    """

    def __init__(self, f, BW=None, dim=None, rho=0.6,
                 az0=0, el0=0, h=0,
                 SL=-20):

        # Define basic parameters from inputs
        self.f = f
        self.lam = c/f
        self.rho = rho
        self.h = h

        self.SL = dB2power(SL)

        # Define beam steering
        self.az0 = az0*pi/180
        self.el0 = el0*pi/180

        # If user defines beamwidth(s), BW
        if BW is not None:

            # If only one beamwidth specified, assume other direction is same
            if isinstance(BW, int):
                BW = [BW, BW]
                self.type = 'circular'
            self.BW = [BW[0]*pi/180, BW[1]*pi/180]
            self.type = 'rect'

            # Calculate directivity G
            self.G = 25000/(self.BW[0]*180/pi*self.BW[1]*180/pi)
            self.A_e = self.G*self.lam**2/(4*pi)
            self.A_ant = self.A_e/rho

            # Calculate antenna size based on ratio of beamwidths
            dim_ratio = [1/self.BW[0], 1/self.BW[1]]
            x = sqrt(self.A_ant*(self.BW[0]*self.BW[1]))
            self.dim = [dim_ratio[0]*x, dim_ratio[1]*x]

        # If user defines antenna dimensions
        elif dim is not None:
            self.dim = dim

            # For circular antenna
            if isinstance(dim, int) or isinstance(dim, float):
                self.type = 'circular'
                self.A_ant = pi*(dim/2)**2
                self.A_e = rho*self.A_ant
                self.G = 4*pi*self.A_e/self.lam**2

                # Calculate beamwidth based on antenna dimensions
                x = sqrt(25000/self.G)*pi/180
                self.BW = [x, x]

            # For rectangular antenna
            elif len(dim) == 2:
                self.type = 'rect'
                self.A_ant = dim[0]*dim[1]
                self.A_e = rho*self.A_ant
                self.G = 4*pi*self.A_e/self.lam**2

                # Calculate beamwidth based on antenna dimensions
                dim_ratio = [1/dim[0], 1/dim[1]]
                x = sqrt(25000/self.G*dim[1]/dim[0])*pi/180
                self.BW = [x, dim[0]/dim[1]*x]

    def steer(self, az0=None, el0=None):
        """
        Steers antenna to specified azimuth and elevation

        Parameters
        ----------
        az0 : float
            azimuth steering vector given in deg
        el0 : float
            elevation steering vector given in deg

        Returns
        -------
        none

        """

        if az0 is not None:
            self.az0 = az0*pi/180

        if el0 is not None:
            self.el0 = el0*pi/180

    def mba(self, az=0, el=0):
        """
        Returns directivty based on Gaussian main beam approximation (MBA)

        Parameters
        ----------
        az : float
            azimuth (deg) at which directivity is approximated
        el : float
            elevation (deg) at which directivity is approximated

        Returns
        -------
        none

        """
        return exp(-2.77*(az*pi/180/self.BW[0])**2), \
            exp(-2.77*(el*pi/180/self.BW[1])**2)

    def get_BW(self, unit='deg'):
        """
        Return antenna half power beamwidth

        Parameters
        ----------
        unit  : {'deg' (default), 'rad', 'sines'}
            units of half power beam width

        Returns
        -------
        beamwidth : list
            beamwidths in azimuth and elevation directions

        """
        # If a Basic Antenna, return the beamwidths defining antenna
        if unit is 'deg':
            return [self.BW[0]*180/pi, self.BW[1]*180/pi]
        elif unit is 'rad':
            return self.BW
        elif unit is 'sines':
            return [sin(self.BW[0]), sin(self.BW[1])]

    def get_G(self, dB=True):
        """
        Return directivity (maximum directivity)

        Parameters
        ----------
        dB : Boolean (default False)
            return directivity in dB

        Returns
        -------
        directivity : float
            maximum directivity of antenna pattern

        """

        if dB:
            return power2dB(self.G)
        else:
            return self.G


class Array():
    '''
    Base class for Linear and Planar Array antennas

    Parameters
    ----------
    f : float, optional
        operating frequency in Hz
    d2wl: float or list of 2 floats, optional
        ratio of element spacing in m to wavelength in m
    d : float or list of 2 floats, optional
        element spacing in m
    L : float or list of 2 floats, optional
        array length or (width and height) in m
    N : int or list of ints, optional
        number of elements in array
    packing : {'rect' or 'tri' (default)}, optional
        element packing of array
    rx_weight: array, optional
        amplitude weighitng for recieve
    Pel : float
        element power in W
    npsb : int (0 default), optional
        number of phase shifter bits
    h : float, optional
        antenna (phase center) height
    orient_az : float (0 default), optional
        array tilt from vertical, in deg
    orient_el : float (0 default), optional
        array azimuth from North in deg

    Methods
    ----------
    plot_pattern()
        plot radiation or directivity pattern
    get_BW()
        return half power beam width
    get_G()
        return directivity, G

    Returns
    -------
    Array object
    '''

    def __init__(self, f=nan,
                 d2wl=None, d=None, L=None, N=None, packing='tri',
                 rx_weight=None, Pel=None, npsb=0,
                 h=0, orient_az=0, orient_el=0, squint=None):

        # operatinoal related parameters
        self.f = f
        self.lam = c/self.f
        self.packing = packing
        self.u0 = 0
        self.v0 = 0
        self.Pel = Pel
        self.npsb = npsb

        # physical parameters
        self.h = h
        self.orient_az = orient_az*pi/180
        self.orient_el = orient_el*pi/180
        self.squint = squint*pi/180 if squint is not None else None

        # parse combinations of d, d2wl, L, and N
        if d is None and d2wl is None and L is None and N is None:
            raise AttributeError("Must specify d2wl and L")
        elif d is None and d2wl is None and L is None and N is not None:
            raise AttributeError("Must specify L, d/lambda, or d with N")
        elif d is None and d2wl is None and L is not None and N is None:
            raise AttributeError("Must specify N, d/lambda, or d with L")
        elif d is None and d2wl is None and L is not None and N is not None:
            self.N = np.array(N)
            self.L = np.array(L)
            self.d = self.L/self.N
            self.d2wl = self.d/self.lam
        elif d is None and d2wl is not None and L is None and N is None:
            raise AttributeError("Must specify L or N, with d/lambda")
        elif d is None and d2wl is not None and L is None and N is not None:
            self.N = np.array(N)
            self.d2wl = np.array(d2wl)
            self.d = self.d2wl*self.lam
            self.L = self.d*self.N
        elif d is None and d2wl is not None and L is not None and N is None:
            self.L = np.array(L)
            self.d2wl = np.array(d2wl)
            self.d = self.d2wl*self.lam
            self.N = self.L//self.d
        elif d is None and d2wl is not None and \
                L is not None and N is not None:
            self.N = np.array(N)
            self.L = np.array(L)
            self.d2wl = np.array(d2wl)
            self.d = self.L/self.N
        elif d is not None and d2wl is None and L is None and N is None:
            raise AttributeError("Must specify L or N with d")
        elif d is not None and d2wl is None and L is None and N is not None:
            self.N = np.array(N)
            self.d = np.array(d)
            self.L = self.d*self.N
            self.d2wl = self.d/self.lam
        elif d is not None and d2wl is None and L is not None and N is None:
            self.L = np.array(L)
            self.d = np.array(d)
            self.N = self.L//self.d
            self.d2wl = self.d/self.lam
        elif d is not None and d2wl is None and \
                L is not None and N is not None:
            self.N = np.array(N)
            self.L = np.array(L)
            self.d = np.array(d)
            self.d2wl = self.d/self.lam
        elif d is not None and d2wl is not None and L is None and N is None:
            raise AttributeError("Must specify L or N with d or d/lambda")
        elif d is not None and d2wl is not None and \
                L is None and N is not None:
            self.N = np.array(N)
            self.d2wl = np.array(d2wl)
            self.d = np.array(d)
            self.L = self.d*self.N
        elif d is not None and d2wl is not None and \
                L is not None and N is None:
            self.L = np.array(L)
            self.d2wl = np.array(d2wl)
            self.d = np.array(d)
            self.N = self.L//self.d
        elif d is not None and d2wl is not None and \
                L is not None and N is not None:
            self.N = np.array(N)
            self.L = np.array(L)
            self.d2wl = np.array(d2wl)
            self.d = np.array(d)

        # Calculate parameters for array and packing type
        # Linear Array
        if self.N.size == 1:
            self.Nel = self.N
            self.amn = 1/sqrt(self.N)*np.ones((self.N, 1))
            self.packing = None
#        elif packing == 'rect' or packing == 'tri' and d is None:
        elif packing == 'rect':
            self.Nel = np.prod(self.N).astype(int)
            self.amn = np.ones((self.N[1], self.N[0]))
        elif packing == 'tri':
            if N is None:
                self.N = ((2*self.L-self.d)//(2*self.d)).astype(int)
                self.Nel = (self.L[1]//(2*self.d[1])) * \
                           (self.L[0]//(2*self.d[0])) + \
                           (self.L[1]-self.d[1])//(2*self.d[1]) * \
                           (self.L[0]-self.d[0])//(2*self.d[0]).astype(int)
            else:
                self.Nel =  np.ceil(self.N[0]/2)*np.ceil(self.N[1]/2)
            self.amn = np.ones((self.N[1], self.N[0]))
            for row in range(self.N[1]):
                # removed diagonal components
                self.amn[row, row % 2::2] = 0
        else:
            raise Exception('Invalid packing. Please user \'rect\' or \'tri\'')

        # force N as an integer number of elements
        self.N = self.N.astype(int)
        self.Nel = self.Nel.astype(int)

        # Calculate antenna patterns
        self.calc_pattern()

    def plot_pattern(self, pat='G', ang_unit='deg', dB=True,
                     norm=True, polar=False, newFig=True, cut=True):
        """
        Plot antenna pattern

        Parameters
        ----------
        pat : 'G' (default) or 'R'
            'R' will plot the radiation pattern
            'G' will plot the directivity pattern
        ang_unit : 'deg' (default), 'rad', or 'sines'
            'deg' will plot pattern against angles in degrees
            'rad' will plot pattern against angles in radians
            'sines' will plot patte0nr against sines
        dB  : Boolean (default True)
            False will plot pattern on a linear y scale
            True will plot pattern on a logarithmic y scale
        norm : Boolean (default True)
            True will normalize pattern before plotting
            False will not normalize pattern before plotting
        polar : Boolean (default False)
            False will plot image of full antenna pattern (if applicable)
            True will plot azimuth and elevation cuts of antenna pattern
        newFig : Boolean (default True)
            plot in new figure

        Returns
        -------
        none

        """

        # Get angle axis
        if isinstance(self, Linear):
            if ang_unit == 'deg' and not polar:
                x = (self.eps + self.orient_az)*180/pi
                xlabel = '$\epsilon$ (deg)'
            elif ang_unit == 'rad' or polar:
                x = (self.eps + self.orient_az)
                xlabel = '$\epsilon$ (rad)'
            elif ang_unit == 'sines' and not polar:
                x = sin(self.eps + self.orient_az)
                xlabel = '$\epsilon$ (sines)'
        elif isinstance(self, Planar):
            if ang_unit == 'deg' and not polar:
                az, el = uv2azel(self.u, self.v)
                x = (az + self.orient_az)*180/pi
                x1 = (el + self.orient_el)*180/pi
                xlabel, xlabel1 = 'Azimuth (deg)', 'Elevation (deg)'
            elif ang_unit is 'rad' or polar:
                az, el = uv2azel(self.u, self.v)
                x = (az + self.orient_az)
                x1 = (el + self.orient_el)
                xlabel, xlabel1 = 'Azimuth (rad)', 'Elevation (rad)'
            elif ang_unit is 'sines' and not polar:
                x, x1 = self.u, self.v
                xlabel, xlabel1 = 'u (sines)', 'v (sines)'

        # Determine which pattern to plot
        if pat is 'G':
            y = self.G
            ylabel = 'Directivity'
        elif pat is 'R':
            y = self.R
            ylabel = 'Radiation Intensity'
        else:
            raise ValueError('Invalid pattern argument')

        # Normalize if desired
        if norm:
            y = y/max(y)

        # Convert to dB if desired
        if dB:
            y = power2dB(y)
            yunit = '(dB)'
            ylim_min = max(y) - 60
        else:
            yunit = '(W/W)'
            ylim_min = 0

        # Plot either polar or "image" of antenna pattern. If plotting image
        # for planar array, use pcolormesh for nonlinear angle axes
        if newFig:
            plt.figure()

        if polar:
            if isinstance(self, Linear):
                plt.polar(x, y)
                plt.gca().set_rmin(ylim_min)
            else:
                y_az = y[fci(x, 0), :].reshape((-1, 1))
                y_el = y[:, fci(x1, 0)].reshape((-1, 1))
                az_cut, = plt.polar(x.reshape(-1, 1), y_az, label='Azimuth')
                el_cut, = plt.polar(x.reshape(-1, 1), y_el, label='Elevation')
                plt.gca().set_rmin(ylim_min)
                plt.legend(handles=[az_cut, el_cut])
        else:
            if isinstance(self, Linear):
                plt.plot(x.reshape(-1, 1), y.reshape(-1, 1))
                plt.xlabel(xlabel)
                plt.ylabel(ylabel + ' ' + yunit)
                plt.ylim(ymin=ylim_min)
                plt.grid('on')

            else:
                X, X1 = np.meshgrid(x, x1)
                if ang_unit == 'deg':
                    X = np.ma.masked_array(X, np.sqrt(X**2+X1**2) > 90)
                    X1 = np.ma.masked_array(X1, np.sqrt(X**2+X1**2) > 90)
                elif ang_unit == 'rad':
                    X = np.ma.masked_array(X, np.sqrt(X**2+X1**2 > pi/2))
                    X1 = np.ma.masked_array(X1, np.sqrt(X**2+X1**2 > pi/2))
                plt.pcolormesh(X, X1, y, vmin=ylim_min)
                plt.gca().axis('equal')
                plt.xlim(xmin=min(X), xmax=max(X))
                plt.ylim(ymin=min(X1), ymax=max(X1))
                plt.xlabel(xlabel)
                plt.ylabel(xlabel1)

    def get_BW(self, level=3, unit='deg'):
        """
        Return antenna half power beamwidth

        Since edge conditions may not yield a point in the radiation pattern
        that drops below level, append the radiation pattern with itself
        (wrap) it so measurements can be made continuously

        Parameters
        ----------
        level : float (3 dB default)
            level where beamwidth measurement occurs
        unit  : {'deg' (default), 'rad', 'sines'}
            space where beamwidth is measured and units of returned beamwidth

        Returns
        -------
        beamwidth : array
            beamwidths in azimuth and elevation directions

        """
        # convert to linear scale
        level = dB2power(-level)

        # Calculate beamwidth based on pattern
        # Append 3 copies of antenna pattern, then find where greater than
        # specified level, then report beamwidth

        if isinstance(self, Linear):
            i_steer = fci(sin(self.eps), self.u0) + self.eps.size
            u = np.array((sin(self.eps)-1,
                          sin(self.eps),
                          sin(self.eps)+1)).reshape((-1, 1))
            R = np.array(list(self.R/max(self.R))*3)
            i_mb_upper = min(i_steer + nonzero(R[i_steer:] <= level)[0])
            i_mb_lower = max(nonzero(R[:i_steer] <= level))
            BW = sp.ptp(u[i_mb_lower:i_mb_upper])

        elif isinstance(self, Planar):
            i_steer = fci(self.u, self.u0) + self.u.size
            u = np.array((sin(self.u)-1,
                          sin(self.u),
                          sin(self.u)+1)).reshape((-1, 1))
            R = (self.R[fci(self.v, self.v0), :]/max(self.R)).reshape((-1, 1))
            R = np.repeat(R, 3, axis=1).T.reshape((-1, 1))
            i_mb_upper = min(i_steer + nonzero(R[i_steer:] <= level)[0])
            i_mb_lower = max(nonzero(R[:i_steer] <= level))
            BW_u = sp.ptp(u[i_mb_lower:i_mb_upper])

            i_steer = fci(self.v, self.v0) + self.u.size
            v = np.array((sin(self.v)-1,
                          sin(self.v),
                          sin(self.v)+1)).reshape((-1, 1))
            R = (self.R[:, fci(self.u, self.u0)]/max(self.R)).reshape((-1, 1))
            R = np.repeat(R, 3, axis=1).T.reshape((-1, 1))
            i_mb_upper = min(i_steer + nonzero(R[i_steer:] <= level)[0])
            i_mb_lower = max(nonzero(R[:i_steer] <= level))
            BW_v = sp.ptp(v[i_mb_lower:i_mb_upper])

            BW = [BW_u, BW_v]

        if unit is 'deg':
            return np.arcsin(BW)*180/pi
        elif unit is 'rad':
            return np.arcsin(BW)
        elif unit is 'sines':
            return BW

    def get_G(self, dB=True):
        """
        Return directivity (maximum directivity)

        Parameters
        ----------
        dB : Boolean (default True)
            return directivity

        Returns
        -------
        directivity : float
            maximum directivity of antenna pattern

        """

        if dB:
            return power2dB(max(max(self.G)))
        else:
            return max(max(self.G))


class Linear(Array):
    """
    Linear Array class

    Methods
    -------
    calc_pattern()
        calculates antenna pattern
    plot_weights()
        plot of T/R module amplitude weighting function
    plot_phase()
        plot of T/R module phase shifter phases
    set_weight()
        set amplitude weighting function
    steer()
        set beam pointing location
    get_gl()
        return grating lobe locations
    get_max_steer()
        return maximum steering angle before grating lobes enter visible space
    get_opt_d2wl()
        return optimum d/lambda for desired maximum steer angle

    Returns
    -------
    Linear object

    """

    def calc_pattern(self, os=8):
        """
        calculate antenna radiation and directivity pattern using FFT method

        Parameters
        ----------
        os : int (8 default)
            oversample factor for antenna pattern

        Returns
        -------
        none

        """

        # upsample if needed
        # amplitude weighting
        fact = np.ceil(2*self.d2wl)
        amn = upsamp(self.amn, fact).reshape((-1, 1))

        # phase taper
        phi = np.arange(amn.size)*2*pi*self.d2wl/fact*self.u0
        self.phi = exp(1j*phi).reshape((-1, 1))

        # quantize phase if desired
        if self.npsb > 0:
            scale = (2*pi/2**self.npsb)
            self.phi = exp(1j*np.round(phi/scale)*scale).reshape((-1, 1))

        # take 2d fft
        nfft = os*nextpow2(max(amn.shape))
        self.A = fftshift(fft(amn*self.phi, nfft, axis=0))

        # transponse for plotting later
        self.eps = fftshift(fftfreq(nfft, d=self.d2wl/fact)).reshape((-1, 1))

        # Calculate radiation pattern
        self.R = np.abs(self.A)**2
        self.Rbar = 1/2*np.trapz(self.R*cos(self.eps),
                                 dx=self.eps[1]-self.eps[0], axis=0)
        self.eps = np.arcsin(self.eps)
        self.G = self.R/self.Rbar

    def calc_monopulse_response(self, dels):
        """
        calculate antenna radiation and directivity pattern using FFT method

        Parameters
        ----------
        os : int (8 default)
            oversample factor for antenna pattern
        sum_patt
        diffm_patt

        Returns
        -------
        none

        """

        # Compute Sum an Difference Patterns
        Va = exp(-1j*pi*(self.N-1)*self.d2wl*(dels)) * \
              sin(pi*self.N*self.d2wl*(dels-sin(self.squint))) / \
              sin(pi*self.d2wl*(dels-sin(self.squint)))
        Vb = exp(-1j*pi*(self.N-1)*self.d2wl*(dels)) * \
              sin(pi*self.N*self.d2wl*(dels+sin(self.squint))) / \
              sin(pi*self.d2wl*(dels+sin(self.squint)))
        Vpeak = 2*sin(pi*self.N*self.d2wl*(sin(self.squint))) / \
                sin(pi*self.d2wl*(sin(self.squint)))
        Vdelta = (Va-Vb)/Vpeak
        Vsum = (Va+Vb)/Vpeak

        return Vsum, Vdelta

    def plot_monpulse_pattern(self, pat='G', ang_unit='deg', dB=True,
                     polar=False, newFig=True, cut=True):
        """
        Plot antenna pattern

        Parameters
        ----------
        pat : 'G' (default) or 'R'
            'R' will plot the radiation pattern
            'G' will plot the directivity pattern
        ang_unit : 'deg' (default), 'rad', or 'sines'
            'deg' will plot pattern against angles in degrees
            'rad' will plot pattern against angles in radians
            'sines' will plot patte0nr against sines
        dB  : Boolean (default True)
            False will plot pattern on a linear y scale
            True will plot pattern on a logarithmic y scale
        norm : Boolean (default True)
            True will normalize pattern before plotting
            False will not normalize pattern before plotting
        polar : Boolean (default False)
            False will plot image of full antenna pattern (if applicable)
            True will plot azimuth and elevation cuts of antenna pattern
        newFig : Boolean (default True)
            plot in new figure

        Returns
        -------
        none

        """

        dels = np.linspace(-0.2, 0.2, 101)

        # Calculate pattern
        Va = sin(pi*self.N*self.d2wl*(dels-sin(self.squint))) / \
             sin(pi*self.d2wl*(dels-sin(self.squint)))
        Vb = sin(pi*self.N*self.d2wl*(dels+sin(self.squint))) / \
             sin(pi*self.d2wl*(dels+sin(self.squint)))

        Vpeak = 2*sin(pi*self.N*self.d2wl*(sin(self.squint))) / \
                sin(pi*self.d2wl*(sin(self.squint)))
        Vdelta = (Va-Vb)/Vpeak
        Vsum = (Va+Vb)/Vpeak

        # Get angle axis
        if isinstance(self, Linear):
            if ang_unit == 'deg' and not polar:
                x = (dels + self.orient_az)*180/pi
                xlabel = '$\Delta$s (deg)'
            elif ang_unit == 'rad' or polar:
                x = (dels + self.orient_az)
                xlabel = '$\Delta$s (rad)'
            elif ang_unit == 'sines' and not polar:
                x = sin(dels + self.orient_az)
                xlabel = '$\Delta$s (sines)'
        elif isinstance(self, Planar):
            if ang_unit == 'deg' and not polar:
                az, el = uv2azel(self.u, self.v)
                x = (az + self.orient_az)*180/pi
                x1 = (el + self.orient_el)*180/pi
                xlabel, xlabel1 = '$\Delta$Azimuth (deg)', '$\Delta$Elevation (deg)'
            elif ang_unit is 'rad' or polar:
                az, el = uv2azel(self.u, self.v)
                x = (az + self.orient_az)
                x1 = (el + self.orient_el)
                xlabel, xlabel1 = '$\Delta$Azimuth (rad)', '$\Delta$Elevation (rad)'
            elif ang_unit is 'sines' and not polar:
                x, x1 = self.u, self.v
                xlabel, xlabel1 = '$\Delta$u (sines)', '$\Delta$v (sines)'

        # Determine which pattern to plot
        y = np.append(Vsum.reshape((-1, 1)), Vdelta.reshape((-1, 1)), axis=1)
        if pat is 'G':
            y = y**2
            ylabel = 'Directivity'
        elif pat is 'V':
            ylabel = 'Amplitude '
        else:
            raise ValueError('Invalid pattern argument')

        # Convert to dB if desired
        if dB:
            y = power2dB(y)
            yunit = '(dB)'
            ylim_min = max(y) - 60
        else:
            yunit = '(W/W)'
            ylim_min = -max(y)

        # Plot either polar or "image" of antenna pattern. If plotting image
        # for planar array, use pcolormesh for nonlinear angle axes
        if newFig:
            plt.figure()

        if polar:
            if isinstance(self, Linear):
                plt.polar(x, y)
                plt.gca().set_rmin(ylim_min)
            else:
                y_az = y[fci(x, 0), :].reshape((-1, 1))
                y_el = y[:, fci(x1, 0)].reshape((-1, 1))
                az_cut, = plt.polar(x.reshape(-1, 1), y_az, label='Azimuth')
                el_cut, = plt.polar(x.reshape(-1, 1), y_el, label='Elevation')
                plt.gca().set_rmin(ylim_min)
                plt.legend(handles=[az_cut, el_cut])
        else:
            if isinstance(self, Linear):
                plt.plot(x.reshape(-1, 1), y)
                plt.xlabel(xlabel)
                plt.ylabel(ylabel + ' ' + yunit)
                plt.ylim(ymin=ylim_min)
                plt.grid('on')

            else:
                X, X1 = np.meshgrid(x, x1)
                if ang_unit == 'deg':
                    X = np.ma.masked_array(X, np.sqrt(X**2+X1**2) > 90)
                    X1 = np.ma.masked_array(X1, np.sqrt(X**2+X1**2) > 90)
                elif ang_unit == 'rad':
                    X = np.ma.masked_array(X, np.sqrt(X**2+X1**2 > pi/2))
                    X1 = np.ma.masked_array(X1, np.sqrt(X**2+X1**2 > pi/2))
                plt.pcolormesh(X, X1, y, vmin=ylim_min)
                plt.gca().axis('equal')
                plt.xlim(xmin=min(X), xmax=max(X))
                plt.ylim(ymin=min(X1), ymax=max(X1))
                plt.xlabel(xlabel)
                plt.ylabel(xlabel1)

    def plot_weights(self, newFig=True):
        """
        plot element amplitude weights

        Parameters
        ----------
        newFig : Boolean
            plot in new figure

        Returns
        -------
        none

        """
        if newFig:
            plt.figure()

        plt.plot(self.amn, '.-')
        plt.xlabel('Element')
        plt.ylabel('Amplitude (V)')
        plt.grid()

    def plot_phase(self, newFig=True):
        """
        plot element phase shifts

        Parameters
        ----------
        newFig : Boolean
            plot in new figure

        Returns
        -------
        none

        """
        if newFig:
            plt.figure()

        plt.plot(np.angle(self.phi)*180/pi, '.-')
        plt.plot(np.unwrap(np.angle(self.phi), axis=0)*180/pi, '.-')
        plt.xlabel('Element')
        plt.ylabel('Phase (deg)')
        plt.grid()

    def set_weight(self, A):
        """
        Apply amplitude weighting A to array

        Parameters
        ----------
        A : array
            amplitude weighting array for each element in array

        Returns
        -------
        none

        """
        self.amn = (1/sqrt(self.N)*A).reshape((-1, 1))
        self.calc_pattern()

    def steer(self, az0=None, u0=None, units='deg'):
        """
        Applies phase steering of array to desired angle

        Parameters
        ----------
        az0 : float
            azimuth steering vector given in units (default deg)
        u0 : float
            u steering vector given in sines
        units : {'deg' (default), 'rad', 'sines'}
            units of steering vector.
            if u0 and v0 specified, units is ignored

        Returns
        -------
        none

        """

        if az0 is not None:
            self.az0 = az0
            if units is 'deg':
                self.az0 = az0*pi/180

            self.u0 = sin(self.az0)
        else:
            self.u0 = u0
            self.az0 = np.arcsin(u0)

        self.calc_pattern()

    def get_gl(self, p=1):
        """
        Return grating lobe locations

        Parameters
        ----------
        p : int
            number of grating lobes left and right of main beam to calculate

        Returns
        -------
        list
            grating lobe locations

        """
        p = np.arange(-p, p+1)
        p = p[p != 0]
        return p/self.d2wl

    def get_max_steer(self, units='deg'):
        """
        Return the max steering angle before grating lobes enter visible space

        Parameters
        ----------
        units: {'deg' (default), 'rad', 'sines'}

        Returns
        -------
        list
            maximum steering vector

        """
        if units == 'deg':
            return np.arcsin(np.abs(1/self.d2wl-1))*180/pi
        elif units == 'rad':
            return np.arcsin(np.abs(1/self.d2wl-1))
        elif units == 'sines':
            return np.abs(1/self.d2wl-1)

    def get_opt_d2wl(self, max_scan=60):
        """
        Return the maximum d/lambda ratio for maximum scan angle

        Parameters
        ----------
        max_scan : float (60 deg default)
            max scan angle

        Returns
        -------
        float
            optimum d/lambda

        """
        return 1/(1+np.sin(max_scan*pi/180))


class Planar(Array):
    """
    Planar Array class

    Methods
    -------
    calc_pattern()
        calculates antenna pattern
    plot_weights()
        plot of T/R module amplitude weighting function
    plot_phase()
        plot of T/R module phase shifter phases
    set_weight()
        set amplitude weighting function
    steer()
        set beam pointing location
    get_gl()
        return grating lobe locations
    plot_gl()
        plot grating lobe locations and boundaries
    get_max_steer()
        return maximum steering angle before grating lobes enter visible space

    Returns
    -------
    Planar object

    """
    def calc_pattern(self, os=8):
        """
        calculate antenna radiation and directivity pattern using FFT method

        Parameters
        ----------
        os : int (8 default)
            oversample factor for antenna pattern

        Returns
        -------
        none

        """

        # upsample if needed
        fact = np.ceil(2*self.d2wl)
        amn = upsamp(self.amn, fact)

        # phase taper
        phi_x = np.arange(amn.shape[1])*2*pi*self.d2wl[1]/fact[1]*self.u0
        phi_y = np.arange(amn.shape[0])*2*pi*self.d2wl[0]/fact[0]*self.v0
        self.phi = np.matmul(exp(1j*phi_y).reshape((-1, 1)),
                             exp(1j*phi_x).reshape((1, -1)))

        # quantize phase if desired
        if self.npsb > 0:
            scale = (2*pi/2**self.npsb)
            phi = np.angle(self.phi)
            self.phi = exp(1j*np.round(phi/scale)*scale)

        # take 2d fft
        nfft = os*nextpow2(max(amn.shape))
        self.A = fftshift(fft2(amn*self.phi, (nfft, nfft)))

        # transponse for plotting later
        self.u = fftshift(fftfreq(nfft, self.d2wl[1]/fact[1])).reshape((-1, 1))
        self.v = fftshift(fftfreq(nfft, self.d2wl[0]/fact[0])).reshape((-1, 1))

        # Mask off portions of antenna pattern outside of visible space
        U, V = np.meshgrid(self.u, self.v)
        self.A = np.ma.masked_array(self.A, (U**2 + V**2).T > 1)

        # Calculate radiation pattern
        self.R = np.abs(self.A)**2
        self.G = self.R/np.max(np.max(self.R))
        # self.Rbar = np.trapz(np.trapz(self.G/
        #                      np.real(sp.sqrt(1-U**2-V**2)).T)) * \
        #     np.prod(1/(2*nfft*self.d2wl/fact))*1/(4*pi)
        self.Rbar = np.trapz(np.trapz(self.R/np.real(sp.sqrt(1-U**2-V**2)).T,
                                      x=self.v, axis=0).reshape((-1, 1)),
                             x=self.u, axis=0) * 1/(4*pi)
        self.G = self.G/self.Rbar

    def plot_weights(self, newFig=True):
        """
        plot element amplitude weights

        Parameters
        ----------
        newFig : Boolean
            plot in new figure

        Returns
        -------
        none

        """

        if newFig:
            plt.figure()

        plt.imshow(self.amn, vmin=0)
        plt.xlabel('Element, x')
        plt.ylabel('Element, y')

    def plot_phase(self, newFig=True):
        """
        plot element phase shifts

        Parameters
        ----------
        newFig : Boolean
            plot in new figure

        Returns
        -------
        none

        """
        if newFig:
            plt.figure()

        plt.imshow(np.angle(self.phi)*180/pi)
#        plt.plot(np.unwrap(np.angle(self.phi), axis=0)*180/pi)
        plt.xlabel('Element, x')
        plt.ylabel('Element, y')

    def set_weight(self, A):
        """
        Apply amplitude weighting A to array

        Parameters
        ----------
        A : array
            amplitude weighting array for each element in array

        Returns
        -------
        none

        """
        if not len(A) == self.N.size:
            raise ValueError("Amplitude weighting vector must have shape as N")

        self.amn = np.matmul(A[1].reshape((-1, 1)), A[0].reshape((1, -1)))
        self.calc_pattern()

    def steer(self, az0=None, el0=None, u0=None, v0=None, units='deg'):
        """
        Applies phase steering of array to desired angle

        Parameters
        ----------
        az0 : float
            azimuth steering vector given in units (default deg)
        el0 : float
            elevation steering vector given in units (default deg)
        u0 : float
            u steering vector given in sines
        v0 : float
            azimuth steering vector given in sines
        units : {'deg' (default), 'rad', 'sines'}
            units of steering vector.
            if u0 and v0 specified, units is ignored

        Returns
        -------
        none

        """

        if az0 is not None and el0 is not None:
            if units is 'deg':
                self.az0 = az0*pi/180
                self.el0 = el0*pi/180
            elif units is 'rad':
                self.az0 = az0
                self.el0 = el0

            if self.N[0] == 1:
                self.az0 = 0
            elif self.N[1] == 1:
                self.el0 = 0
            self.u0, self.v0 = azel2uv(self.az0, self.el0)
        else:
            self.u0 = u0
            self.v0 = v0
            self.az0, self.el0 = uv2azel(self.u0, self.v0)

        self.calc_pattern()

    def get_gl(self, p=3, q=3):
        """
        Return grating lobe locations

        Parameters
        ----------
        p : int
            number of grating lobes left and right of main beam to calculate
        q : int
            number of grating lobes above and below main beam to calculate

        Returns
        -------
        ug, vg : list
            list of arrays of grating lobe locations

        """
        p = np.arange(-p, p+1)
        q = np.arange(-q, q+1)
        P, Q = np.meshgrid(p, q)

        if self.packing.lower() == 'rect':
            return P/self.d2wl[0], Q/self.d2wl[1]
        elif self.packing.lower() == 'tri':
            P[np.mod(abs(P + Q), 2) == 1] = 0
            Q[np.mod(abs(P + Q), 2) == 1] = 0
            return P/(2*self.d2wl[0]), Q/(2*self.d2wl[1])

    def plot_gl(self, p=3, q=3, plot_uc=False, newFig=True):
        """
        Plot grating lobe locations for array

        Parameters
        ----------
        p : int
            number of grating lobes left and right of main beam to calculate
        q : int
            number of grating lobes above and below main beam to calculate
        plot_uc: Boolean (default False)
            plot unit circle (visible space) on grating lobe plot
        newFig : Boolean (default True)
            plot in new figure

        Returns
        -------
        none

        """
        ug, vg = self.get_gl(p, q)
        ug = np.reshape(ug, (-1, 1))
        vg = np.reshape(vg, (-1, 1))

        for i in range(ug.size):
            if ug[i] == 0 and vg[i] == 0:
                pass
            else:
                plt.plot(ug[i], vg[i], 'ks', alpha=0.5)
                unit_circle = plt.Circle((ug[i], vg[i]), 1, fill=False,
                                         color='r')
                plt.gca().add_artist(unit_circle)

        # Plot unit circle
        if plot_uc:
            unit_circle = plt.Circle((self.u0, self.v0), 1, fill=False)
            plt.gca().add_artist(unit_circle)
        plt.gca().axis('equal')

    def get_max_steer(self, units='deg'):
        """
        Return the max steering angle before grating lobes enter visible space

        Parameters
        ----------
        units: {'deg' (default), 'rad', 'sines'}

        Returns
        -------
        u0_max, v0_max or az0_max, el0_max: list
            list of maximum steering vector
            for triangular packing

        """
        if self.packing.lower() == 'rect':
            if units == 'deg':
                if isinstance(self, Planar):
                    return np.arcsin(np.abs(self.lam/self.d[0]-1))*180/pi, \
                           np.arcsin(np.abs(self.lam/self.d[1]-1))*180/pi
                elif isinstance(self, Linear) and self.N[0] == 1:
                    return nan, \
                           np.arcsin(np.abs(self.lam/self.d[1]-1))*180/pi
                elif isinstance(self, Linear) and self.N[1] == 1:
                    return np.arcsin(np.abs(self.lam/self.d[0]-1))*180/pi, \
                           nan
            elif units == 'rad':
                if isinstance(self, Planar):
                    return np.arcsin(np.abs(self.lam/self.d[0]-1)), \
                           np.arcsin(np.abs(self.lam/self.d[1]-1))
                elif isinstance(self, Linear) and self.N[0] == 1:
                    return nan, \
                           np.arcsin(np.abs(self.lam/self.d[1]-1))
                elif isinstance(self, Linear) and self.N[1] == 1:
                    return np.arcsin(np.abs(self.lam/self.d[0]-1)), \
                           nan
            elif units == 'sines':
                if isinstance(self, Planar):
                    return np.abs(self.lam/self.d[0]-1), \
                       np.abs(self.lam/self.d[1]-1)
                elif isinstance(self, Linear) and self.N[0] == 1:
                    return nan, \
                           np.abs(self.lam/self.d[1]-1)
                elif isinstance(self, Linear) and self.N[1] == 1:
                    return np.abs(self.lam/self.d[0]-1), \
                           nan

        elif self.packing.lower() == 'tri':
            if units == 'deg':
                return abs(np.arcsin(1/(sqrt(3)*self.d2wl[0])-1))*180/pi, \
                       abs(np.arcsin(1/self.d2wl[1]-1))*180/pi
            elif units == 'rad':
                return abs(np.arcsin(1/(sqrt(3)*self.d2wl[0])-1)), \
                       abs(np.arcsin(1/self.d2wl[1]-1))
            elif units == 'sines':
                return 1/(sqrt(3)*self.d2wl[0])-1, 1/self.d2wl[1]-1


def cosine(N, edge_taper=20):
    """
    Compute cosine element weighting

    Parameters
    ----------
    N : int
        number of weighting coeffients
    edge_taper : float
        edge taper in dB for computing weighting

    Returns
    -------
    a = array
        array of weighting coeffients

    """

    alpha = np.arccos(10**(-abs(edge_taper)/20))/((N-1)/2)
    print(alpha)
    return cos((np.arange(N)-(N-1)/2)*alpha)


def taylor(N, n_bar=6, SL=30):
    """
    Compute Taylor element weighting

    Parameters
    ----------
    N : int
        number of weighting coeffients
    n_bar : int (default 6)
        number of sidelobes at constant weight
    SL : float (default 30)
        maximum sidelobe level in dB

    Returns
    -------
    a = array
        array of weighting coeffients

    """
    m = n = np.arange(1, n_bar)[np.newaxis]

    R = 10**(SL/20)
    z = np.arange(-N, N+1)/2
    xn = z[1::2][np.newaxis]
    A = np.arccosh(R)/pi
    sigma2 = n_bar**2/(A**2+(n_bar-0.5)**2)

    F = sp.misc.factorial(n_bar-1)**2 \
        * np.prod(1-n**2/(sigma2*(A**2+(m.T-0.5)**2)), axis=0) \
        / (sp.misc.factorial(n_bar-1+n)*sp.misc.factorial(n_bar-1-n))
    a = 1+2*np.sum(F*cos(2*n.T*pi*xn/N).T, axis=1)
    return a/max(a)


def cheb(N, SL=30):
    return sp.signal.chebwin(N, SL)


if __name__ == '__main__':

    print('\nExamples for Antenna module')
    print('-----------------------------')
    print('1. 1.5 m Dish Antenna @ 10 GHz')
    print('2. 1 m x 2 m Rectangular array @ 5 GHz')
    print('3. Antenna with 1 deg x 10 deg BW')
    print('4. 20 element Linear Array @ 1 GHz (Taylor weighting)')
    print('5. 60 x 40 element Planar Array @ 3 GHz (rectangular pack)')
    print('6. 60 x 40 element Planar Array @ 3 GHz (triangular pack)')
    print('7. Linear array examples')
    print('8. Linear array T/R module setting plots')
    print('9. Planar array T/R module setting plots')
    print('0. Exit')
    # choice = input('Select example : ')
    choice = 0

    if choice == '1':

        '''
        ---------------------------------------------------------------------------
        Basic Antenna Example 1

        Create a 1.5 m diameter dish antenna operating at 10 GHz
        '''
        DishAnt = Antenna(f=10e9, dim=1.5)

        # Display all parameters associated with antenna
        print('\nDish Antenna Example 1')
        print(DishAnt.__dict__)

        # Return antenna beamwidth
        print('DishAnt Beamwidth = ' + str(DishAnt.get_BW()) + ' deg')

    elif choice == '2':

        '''
        ---------------------------------------------------------------------------
        Basic Antenna Example 2

        Create a 1 m x 2 m rectangular array operating at 5 GHz
        '''
        RectAnt = Antenna(f=5e9, dim=(1, 2))

        # Display all parameters associated with antenna
        print('\nRectangular Antenna Example 2')
        print(RectAnt.__dict__)

        # Return directivity
        print('RectAnt Directivity = ' + str(RectAnt.get_G(dB=True)) + ' dB')

    elif choice == '3':

        '''
        ---------------------------------------------------------------------------
        Basic Antenna Example 3

        Create an antenna with a 1 deg x 10 deg beamwidth at 15 GHz
        '''
        RectAnt2 = Antenna(f=15e9, BW=(5, 10))

        # Display all parameters associated with antenna
        print('\nRectangular Antenna Example 3')
        print(RectAnt2.__dict__)

    elif choice == '4':

        '''
        ---------------------------------------------------------------------------
        Array Antenna Example 4

        Create a 20 element linear array operating at 1 GHz
        with 0.5 with 30 dB Taylor weighting
        '''
        LinearArray1 = Linear(f=1e9, d2wl=0.5, N=20)
        LinearArray1.plot_pattern(dB=True, norm=True, newFig=True)
        LinearArray1.set_weight(taylor(LinearArray1.N))
        LinearArray1.plot_pattern(dB=True, norm=True, newFig=False)
        plt.legend(('Uniform', 'Taylor'))
        print('LinearArray1 beamwidth = ' + str(LinearArray1.get_BW()) + ' deg')
        print('LinearArray1 directivity = ' + str(LinearArray1.get_G(dB=True)) + ' dB')

    elif choice == '5':

        '''
        ---------------------------------------------------------------------------
        Array Antenna Example 5

        Create a 60 element x 40 element array operating at 3 GHz (rectangular packing)
        '''
        Ant = Planar(f=3e9, d2wl=(0.5, 0.8), N=(60, 40))
        Ant.steer(u0=0.4285, v0=0.25)

        # Plot image of antenna pattern
        Ant.plot_pattern(pat='G', ang_unit='sines', dB=True, norm=True)

        # Plot grating lobe pattern
        Ant.plot_gl()

        # Get maximum steering angles
        print('Maximum steering angles : ' +
              str(Ant.get_max_steer()) + ' deg')

        # Get beamwidth and directivity
        print('Ant beamwidth = ' + str(Ant.get_BW()) + ' deg')
        print('Ant directivity = ' + str(Ant.get_G(dB=True)) + ' dB')

    elif choice == '6':

        '''
        ---------------------------------------------------------------------------
        Array Antenna Example 6

        Create a 60 element x 40 element array operating at 3 GHz (triangular packing)
        '''
        Ant = Planar(f=3e9, d2wl=(0.38, 0.67), N=(60, 40), packing='tri')
        Ant.steer(az0=30, el0=0)

        # Plot image of antenna pattern
        Ant.plot_pattern(pat='G', ang_unit='sines', dB=True, norm=True)

        # Plot grating lobe pattern
        Ant.plot_gl()

        # Get maximum steering angles
        print('Maximum steering angles : ' + str(Ant.get_max_steer()) + ' deg')

    elif choice == '7':
        '''
        Linear array examples
        '''
        Linear(d2wl=0.5, N=20).plot_pattern(polar=False, dB=True, norm=False, ang_unit='deg')
        Linear(d2wl=0.5, N=20).plot_pattern(polar=False, dB=True, norm=False, ang_unit='rad')
        Linear(d2wl=0.5, N=20).plot_pattern(polar=False, dB=True, norm=False, ang_unit='sines')
        Linear(d2wl=0.5, N=20).plot_pattern(polar=False, dB=False, norm=False)
        Linear(d2wl=0.5, N=20).plot_pattern(polar=False, dB=False, norm=True)
        Linear(d2wl=0.5, N=20).plot_pattern(polar=False, dB=True, norm=False)
        Linear(d2wl=0.5, N=20).plot_pattern(polar=False, dB=True, norm=True)
        Linear(d2wl=0.5, N=20).plot_pattern(polar=True, dB=False, norm=False)
        Linear(d2wl=0.5, N=20).plot_pattern(polar=True, dB=False, norm=True)
        Linear(d2wl=0.5, N=20).plot_pattern(polar=True, dB=True, norm=False)
        Linear(d2wl=0.5, N=20).plot_pattern(polar=True, dB=True, norm=True)

    elif choice == '8':
        '''
        Plot phase shifter settings
        '''
        x = Linear(d2wl=0.5, N=51)
        x.steer(-30)
        x.set_weight(taylor(x.N))
        x.plot_pattern(polar=False, dB=True, norm=False, ang_unit='deg', newFig=True)
        x.plot_pattern(polar=False, dB=True, norm=True, ang_unit='deg', newFig=False)
        x.plot_weights()
        x.plot_phase()

    elif choice == '9':
        '''
        Planar Array plots
        '''
        y = Planar(d2wl=(0.8, 0.50), N=(51, 51), packing='tri', npsb=3)
        y.steer(u0=0.1, v0=0.1)
        y.set_weight((taylor(60), taylor(40)))
        y.plot_pattern(polar=False, dB=True, norm=False, newFig=True, ang_unit='sines')
        y.plot_gl()
        y.plot_weights()
        y.plot_phase()
        print(y.get_max_steer('sines'))
        print(y.get_BW(unit='sines'))
        print(y.get_BW(unit='deg'))
