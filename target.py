# -*- coding: utf-8 -*-
"""
Created on Tue Nov 21 13:56:57 2017

@author: Edwards
"""

import numpy as np

from general import dB2power
from _constants import nan


class Target():
    '''
    Target class

    Parameters
    ----------
    R : float
        range (m)
    dr : float
        range rate (m/s)
    sigma : float
        target radar cross section
    '''

    def __init__(self, R=nan, Rdot=nan, Raccel=nan, h=nan, R0=nan, Rdot0=nan, u=nan, v=nan,
                 sigma=0, sigma_dB=True, Rref=nan, SNRref=nan, SNRref_dB=True, sigmaref=nan, sigmaref_dB=True,
                 name='Target'):

        self.name = name

        self.t = 0

        self.R0 = R0
        self.Rdot0 = Rdot0
        self.R = np.array(R)
        self.Rdot = np.array(Rdot)
        self.Raccel = np.array(Raccel)
        self.h = np.array(h)
        self.u = np.array(u)
        self.v = np.array(v)

        if isinstance(R0, np.ndarray) and len(self.R0) > 1:
            raise ValueError('R0 is an initial condition and can only be a single value.')
        if isinstance(R0, np.ndarray) and len(self.Rdot0) > 1:
            raise ValueError('Rdot0 is an initial condition and can only be a single value.')

        self.sigma = sigma if not sigma_dB else dB2power(sigma)
        self.Rref = Rref
        self.SNRref = SNRref if not SNRref_dB else dB2power(SNRref)
        self.sigmaref = sigmaref if not sigmaref_dB else dB2power(sigmaref)

        self.calc_snr()

        self.npoints = self.R.size

    def calc_snr(self):
        if np.any(np.isnan([self.Rref, self.SNRref, self.sigmaref])):
            self.SNR = np.array(self.sigma)
        else:
            Kref = self.SNRref*self.Rref**4/self.sigmaref
            self.SNR = Kref*self.sigma/self.R**4

    def sim_racetrack_traj(self, tmax, T=0.1, tstart=0, r=10000, thetadot=1, theta0=90):
        self.t = np.arange(tstart, tmax, T)
        self.npoints = self.t.size

        thetadot = thetadot*np.pi/180
        theta = thetadot*self.t + theta0*np.pi/180
        self.R = np.sqrt(r**2 + self.R0**2 + 2*r*self.R0*np.sin(theta))
        self.Rdot = self.R*r*np.cos(theta)*thetadot/self.R
        self.Raccel = (-self.R*self.R0*r*np.sin(theta)*(thetadot**2) - self.Rdot*self.R0*r*np.cos(theta)*thetadot)/self.R**2

        x = self.R0 + r*np.sin(theta)
        y = r*np.cos(theta)
        self.u = y/x
        self.v = self.h/x

        self.calc_snr()

    def sim_linear_traj(self, tmax, T=0.1, tstart=0, Rmin=0, u=0):
        self.t = np.arange(tstart, tmax, T)
        self.npoints = self.t.size

        self.R = self.R0 + self.t*self.Rdot0 + Rmin
        if np.all(np.isnan(self.R)):
            raise ValueError('Check input values. Calculated states are NaN.')
        self.Rdot = self.Rdot0*np.ones(self.t.shape)
        self.Raccel = np.zeros(self.t.shape)

        self.u = u*np.ones(self.R.shape) if Rmin == 0 else self.R/Rmin
        self.v = self.h/self.R

        self.calc_snr()

    def get_state(self, n):
        return {'n': n,
                'R': self.R[n] if self.R.size > 1 else self.R,
                'Rdot': self.Rdot[n] if self.Rdot.size > 1 else self.Rdot,
                'Raccel': self.Raccel[n] if self.Raccel.size > 1 else self.Raccel,
                'SNR': self.SNR[n] if self.SNR.size > 1 else self.SNR,
                'u': self.u[n] if self.u.size > 1 else self.u,
                'v': self.v[n] if self.v.size > 1 else self.v
                }