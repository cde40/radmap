# -*- coding: utf-8 -*-
"""
Moduele for calculating radar range equation performance metrics

"""

import numpy as np
import pandas as pd
import scipy as sp
import matplotlib.pyplot as plt
import os

from _constants import k, pi, nan, T0
from general import dB2power, power2dB
from detection import RCS
from target import Target
from antenna import Array


class Radar():
    '''
    Class for Radar object

    Radar object describes performance metrics associated with parameters of
    given radar including waveforms.

    Also takes into account target and clutter parameters

    '''

    def __init__(self,
                 Pt=None, Peff=None, power_dB=True,
                 Ant=None, Gt=None, Gr=None, G=None, g_dB=True,
                 TxWfm=None,
                 sigmaT=None, tgt_sig_dB=True,
                 sigmaC=None, cltr_sig_dB=True,
                 calcR=True, Rmax=100e3, Rres=100, R=None,
                 L=None, Lt=None, Lant=None, Lother=None, l_dB=True,
                 Ts=None, Fn=None, fn_dB=True,
                 phi=-130, phi_dB=True,
                 name='Radar'):

        self.name = name
        if phi_dB:
            self.phi = dB2power(phi)

        # Add waveform to instance waveform library
        if isinstance(TxWfm, tuple):
            self.TxWfm = TxWfm
        else:
            self.TxWfm = (TxWfm, )

        # Antenna related parameters
        if Gt is not None and Gr is not None and Ant is None and G is None:
            self.Gt = Gt
            self.Gr = Gr
            if g_dB:
                self.Gt = dB2power(self.Gt)
                self.Gr = dB2power(self.Gr)
        elif Ant is not None and Gt is None and Gr is None and G is None:
            self.Gt = self.Gr = Ant.get_G()
        elif Ant is None and Gt is None and Gr is None and G is not None:
            self.Gt = G
            self.Gr = G
            if g_dB:
                self.Gt = dB2power(self.Gt)
                self.Gr = dB2power(self.Gr)

        # Loss parameters
        if L is not None and Lt is None and Lant is None and Lother is None:
            self.L = L
            if l_dB:
                self.L = dB2power(self.L)
            self.Lt = nan
            self.Lant = nan
            self.Lother = nan
        elif L is None and Lt is not None and Lant is not None and Lother is not None:
            self.Lant = Lant
            self.Lt = Lt
            self.Lother = Lother
            if l_dB:
                self.Lant = dB2power(self.Lant)
                self.Lt = dB2power(self.Lt)
                self.Lother = dB2power(self.Lother)
            self.L = self.Lant*self.Lt*self.Lother
        else:
            raise AttributeError('Must specify eithe L or Lt, Lant, Lother')

        # Transmit Power
        if Pt is not None and Peff is None:
            self.Pt = Pt
            if power_dB:
                self.Pt = dB2power(self.Pt)

            self.Peff = self.Pt*self.Gt/(self.Lt*self.Lant)
            self.Prad = self.Pt/(self.Lt*self.Lant)

        elif Pt is None and Peff is not None:
            self.Peff = Peff
            if power_dB:
                self.Peff = dB2power(self.Peff)
            self.Pt = self.Peff*(self.Lt*self.Lant)/self.Gt
            self.Prad = self.Pt/(self.Lt*self.Lant)

        elif Pt is None and Peff is None and \
            isinstance(Ant, Array) and Ant.Pel is not None:

            self.Pt = Ant.Nel*Ant.Pel
            self.Prad = self.Pt/(self.Lt*self.Lant)
            self.Peff = self.Pt*self.Gt/(self.Lt*self.Lant)

        # Range
        # Extend range if needed
        if R is not None:
            self.R = np.array(R)
        else:
            iRmax = Rmax
            if calcR:
                Rmax = 0
                for iWfm in list(self.TxWfm):
                    Rmax = max((min((iRmax, iWfm.Rmax*iWfm.ppd)), Rmax))
            self.R = np.arange(Rres, Rmax+Rres, Rres).reshape((-1, 1))

        # Noise related parameters
        self.Fn = Fn
        if Fn is not None and fn_dB:
            self.Fn = dB2power(self.Fn)

        # Temperature related
        # Import Blake's noise temperature for idealized antenna
        # Set up function fTa1 to interpolate for frequency and antenna angle
        if Ts is not None:
            self.Ts = list((Ts, )*len(self.TxWfm))
        else:
            Ta1 = pd.read_csv(os.path.join(os.path.dirname(__file__),
                                           'data',
                                           'antenna_temperature_blake.csv'))
            fTa1 = sp.interpolate.RectBivariateSpline(Ta1['freq']*1e6,
                                                      [0, 1, 2, 5, 10, 30, 90],
                                                      Ta1.drop('freq', axis=1))
            self.Ta1 = []
            self.Ta = []
            self.Ts = []
            for iWfm in self.TxWfm:
                self.Ta1.append(fTa1(iWfm.f, Ant.el0*180/pi))
                self.Ta.append(0.876*self.Ta1[-1] + 36)
                self.Ts.append(self.Ta[-1] + (self.Fn-1)*T0)

        # Target model
        self.set_target(sigmaT, tgt_sig_dB)

        # Clutter model
        self.set_clutter(sigmaC, cltr_sig_dB)

        # Calculate performance curves for each waveform
        self.calc_snr()
        self.calc_cnr()

    def set_target(self, sigma, dB=True):
        '''
        Define target parameters for performance calculations

        Parameter
        ---------
        sigma : int, float, RCS, or Target object
            defines target RCS
        '''

        if isinstance(sigma, float) or isinstance(sigma, int):
            self.sigmaT = sigma
            if dB:
                self.sigmaT = dB2power(sigma)
            self.Target = None
        elif isinstance(sigma, RCS):
            self.sigmaT = sigma.sigma
            self.Target = sigma
        elif isinstance(sigma, Target):
            self.sigmaT = sigma.sigma.sigma
            self.Target = sigma
        else:
            self.sigmaT = 1
            self.Target = None
        self.calc_snr()

    def set_clutter(self, sigma, dB=True):
        '''
        Define clutter parameters for performance calculations

        Parameter
        ---------
        sigma : int, float, RCS, or Clutter object
            defines target RCS
        '''

        if isinstance(sigma, float) or isinstance(sigma, int):
            self.sigmaC = sigma
            if dB:
                self.sigmaC = dB2power(self.sigma)
            self.sigmaC0 = sigma
            self.Clutter = None
        elif isinstance(sigma, RCS):
            sigma.calc_sigmaC(Rmax=np.max(self.R), Rres=self.R[1]-self.R[0])
            self.sigmaC = sigma.sigmaC
            self.sigmaC0 = sigma.sigma0
            self.Clutter = sigma
        else:
            self.sigmaC = 0
            self.sigmaC0 = 0
            self.Clutter = None

        self.calc_cnr()

    def get_ERP(self, dB=True):
        '''
        return ERP

        Parameter
        ---------
        dB : bool (default True)
            if true, returns ERP in dB
        '''
        if dB:
            return power2dB(self.Peff)
        else:
            return self.Peff

    def get_Pel(self, dB=False):
        '''
        return element power

        Parameter
        ---------
        dB : bool (default True)
            if true, returns element power in dB
        '''
        if dB:
            return power2dB(self.Pt/self.Ant.Nel)
        else:
            return self.Pt/self.Ant.Nel

    def get_Nel(self):
        '''
        return number of elements
        '''
        return self.Pt/self.Ant.Pel

    def get_Pt(self, dB=True):
        '''
        return transmit power

        Parameter
        ---------
        dB : bool (default True)
            if true, returns transmit power in dB
        '''
        if dB:
            return power2dB(self.Pt)
        else:
            return self.Pt

    def get_L(self, dB=True):
        '''
        return losses

        Parameter
        ---------
        dB : bool (default True)
            if true, returns losses in dB
        '''
        if dB:
            return power2dB(self.L)
        else:
            return self.L

    def get_G(self, dB=True):
        '''
        return gain

        Parameter
        ---------
        dB : bool (default True)
            if true, returns gain in dB
        '''
        if dB:
            return (power2dB(self.Gt), power2dB(self.Gr))
        else:
            return (self.Gt, self.Gr)

    def get_sigmaT(self, dB=True):
        '''
        return target RCS

        Parameter
        ---------
        dB : bool (default True)
            if true, returns target RCS in dB
        '''
        if dB:
            return power2dB(self.sigmaT)
        else:
            return self.sigmaT

    def get_sigmaC(self, dB=True):
        '''
        return clutter RCS

        Parameter
        ---------
        dB : bool (default True)
            if true, returns clutter RCS in dB
        '''
        if dB:
            return power2dB(self.sigmaC)
        else:
            return self.sigmaC

    def get_SNR(self, dB=True):
        '''
        return calculated SNR

        Parameter
        ---------
        dB : bool (default True)
            if true, returns SNR in dB
        '''
        if dB:
            return power2dB(self.SNR)
        else:
            return self.SNR

    def calc_snr(self):
        """
        Calculate SNR from radar range equation for each waveform

        Parameters
        ----------
        none

        Returns
        -------
        none

        """
        Ps = []
        Pn = []
        self.SNR = []
        for i in range(len(self.TxWfm)):
            Ps = self.Pt*self.Gt*self.Gr*self.TxWfm[i].lam**2*self.sigmaT*self.TxWfm[i].tauP \
                     / ((4*pi)**3*self.R**4*self.L)
            Pn = k*self.Ts[i]
            self.SNR.append(Ps/Pn)

            # implement blanking
            if self.SNR[-1].size > 1:
                R = self.R
                ppd = self.TxWfm[i].ppd
                Ramb = self.TxWfm[i].Ramb
                if np.isnan(Ramb):
                    Ramb = np.max(R)
                Rmin = self.TxWfm[i].Rmin
                if np.isnan(Rmin):
                    Rmin = np.min(R)
                Rmax = self.TxWfm[i].Rmax
                if np.isnan(Rmax):
                    Rmax = np.max(R)
                iR = (np.mod(R, Ramb) >= Rmin) & (np.mod(R, Ramb) <= Rmax) & \
                    (R < ppd*Ramb)

                self.SNR[-1][iR==False] = nan


    def calc_cnr(self):
        """
        Calculate CNR from radar range equation for each waveform

        Parameters
        ----------
        none

        Returns
        -------
        none

        """
        Ps = []
        Pn = []
        self.CNR = []
        for i in range(len(self.TxWfm)):
            Ps = self.Pt*self.Gt*self.Gr*self.TxWfm[i].lam**2*self.sigmaC*self.TxWfm[i].tauP \
                    / ((4*pi)**3*self.R**4*self.L)
            Pn = k*self.Ts[i]
            self.CNR.append(Ps/Pn)

            # implement clutter folding
            if self.TxWfm[i].ppd > 1 and self.Clutter is not None:
                R = self.R
                Ramb = self.TxWfm[i].Ramb
                if np.isnan(Ramb):
                    Ramb = np.max(R)
                Rmin = self.TxWfm[i].Rmin
                if np.isnan(Rmin):
                    Rmin = np.min(R)
                Rmax = self.TxWfm[i].Rmax
                if np.isnan(Rmax):
                    Rmax = np.max(R)
                nPRIs = int(np.max(self.R)//Ramb)

                iR = (np.mod(self.R, Ramb) > Rmin) & \
                    (np.mod(self.R, Ramb) <= Rmax) & \
                    (self.R <= nPRIs*Ramb)

                Pcpd = self.CNR[-1][iR]
                Pcpd = np.sum(Pcpd.reshape((-1, nPRIs)), axis=1)
                self.CNR[-1][iR] = np.array(list(Pcpd)*nPRIs)
                self.CNR[-1][iR==False] = np.nan

    def calc_Rdet(self, SNR_req=13, snr_units='dB'):
        """
        Calculate range at which SNR requirement is achieved for each waveform

        Parameters
        ----------
        SNR_req : float (default 13 dB)
            SNR requirement to be met
        snr_units : {'dB' (default), 'power'}
            input unts of SNR_req

        Returns
        -------
        float
            range (in meters) where SNR requirement is met

        """
        if snr_units is 'dB':
            SNR_req = dB2power(SNR_req)

        Ps = []
        Pn = []
        R_det = []
        for i in range(len(self.TxWfm)):
            Ps = self.Pt*self.Gt*self.Gr*self.TxWfm[i].lam**2*self.sigmaT*self.TxWfm[i].tauP \
                    / ((4*pi)**3*SNR_req*self.L)
            Pn = k*self.Ts[i]
            R_det.append(float((Ps/Pn)**(1/4)))
        return np.array(R_det)

    def plot_snr(self, newFig=True):
        """
        Plot SNR for each waveform vs range

        Parameters
        ----------
        newFig : Boolean (default True)
            create plot in a new figure

        Returns
        -------
        none

        """

        # Make a new figure if desired
        if newFig:
            plt.figure()

        for i in range(len(self.TxWfm)):

            # Get data to plot
            R_to_plot = self.R
            SNR_to_plot = self.SNR[i]

            # Mask off blind range
            if not np.isnan(self.TxWfm[i].Rmin):
                SNR_to_plot[R_to_plot < self.TxWfm[i].Rmin] = nan

            # Mask off max usable range
            if not np.isnan(self.TxWfm[i].Rmax):
                SNR_to_plot[R_to_plot > min((np.max(self.R), self.TxWfm[i].Rmax*self.TxWfm[i].ppd))] = nan

            plt.plot(R_to_plot/1e3, power2dB(SNR_to_plot),label=self.TxWfm[i].name)
        plt.grid('on')
        plt.xlabel('Range (km)')
        plt.ylabel('SNR (dB)')
        try:
            plt.title('SNR vs Range for ' + str(int(power2dB(self.sigma))) + ' dBsm Target')
        except:
            pass
        plt.legend()

    def plot_cnr(self, newFig=True):
        """
        Plot CNR for each waveform vs range

        Parameters
        ----------
        newFig : Boolean (default True)
            create plot in a new figure

        Returns
        -------
        none

        """

        # Make a new figure if desired
        if newFig:
            plt.figure()

        for i in range(len(self.TxWfm)):

            # Get data to plot
            R_to_plot = self.R
            CNR_to_plot = self.CNR[i]

            # Mask off blind range
            if not np.isnan(self.TxWfm[i].Rmin):
                CNR_to_plot[R_to_plot < self.TxWfm[i].Rmin] = nan

            # Mask off max usable range
            if not np.isnan(self.TxWfm[i].Rmax):
                CNR_to_plot[R_to_plot > min((np.max(self.R), self.TxWfm[i].Rmax*self.TxWfm[i].ppd))] = nan

            plt.plot(R_to_plot/1e3, power2dB(CNR_to_plot),label=self.TxWfm[i].name)
        plt.grid('on')
        plt.xlabel('Range (km)')
        plt.ylabel('CNR (dB)')
        try:
            plt.title('CNR vs Range for ' + str(int(power2dB(self.sigma))) + ' dBsm Target')
        except:
            pass
        plt.legend()

    def plot_sir(self, newFig=True):
        """
        Plot SIR for each waveform vs range

        Parameters
        ----------
        newFig : Boolean (default True)
            create plot in a new figure

        Returns
        -------
        none

        """

        # Make a new figure if desired
        if newFig:
            plt.figure()

        for i in range(len(self.TxWfm)):

            # Get data to plot
            R_to_plot = self.R
            SNR_to_plot = self.SNR[i]
            CNR_to_plot = self.CNR[i]

            # Mask off blind range
            if not np.isnan(self.TxWfm[i].Rmin):
                SNR_to_plot[R_to_plot < self.TxWfm[i].Rmin] = nan
                CNR_to_plot[R_to_plot < self.TxWfm[i].Rmin] = nan

            # Mask off max usable range
            if not np.isnan(self.TxWfm[i].Rmax):
                SNR_to_plot[R_to_plot > min((np.max(self.R), self.TxWfm[i].Rmax*self.TxWfm[i].ppd))] = nan
                CNR_to_plot[R_to_plot > min((np.max(self.R), self.TxWfm[i].Rmax*self.TxWfm[i].ppd))] = nan

            plt.plot(R_to_plot/1e3, power2dB(SNR_to_plot/(CNR_to_plot+1)),
                     label=self.TxWfm[i].name)
            plt.grid('on')
            plt.xlabel('Range (km)')
            plt.ylabel('SIR (dB)')
            plt.legend()

        plt.title('SIR vs Range for ' +
                  str(int(power2dB(self.sigmaT))) + ' dBsm Target and ' +
                  str(int(power2dB(self.sigmaC0))) + ' dBsm Clutter')

    def plot_snr_cnr_sir(self, newFig=True):
        """
        Combined plot SNR, CNR, and SIR vs range for each waveform

        Parameters
        ----------
        newFig : Boolean (default True)
            create plot in a new figure

        Returns
        -------
        none

        """

        # Make a new figure if desired
        if newFig:
            plt.figure()

        plt.subplot(3, 1, 1)
        self.plot_snr(newFig=False)
        plt.xlabel('')
        plt.title('SNR, CNR, and SIR vs Range for ' +
                  str(int(power2dB(self.sigmaT))) + ' dBsm Target and ' +
                  str(int(power2dB(self.sigmaC0))) + ' dBsm Clutter')
        plt.subplot(3, 1, 2)
        self.plot_cnr(newFig=False)
        plt.xlabel('')
        plt.title('')
        plt.subplot(3, 1, 3)
        self.plot_sir(newFig=False)
        plt.title('')

if __name__ == '__main__':

    print('\nExamples for Performance module')
    print('-----------------------------')
    print('1. Performance Example - Budge Ch 2.4 Example 1')
    print('2. Performance Example 1')
    print('3. Performance Example 2')
    print('0. Exit')
    choice = input('Select example : ')

    if choice == '1':
        '''
        Example 1 from Budge Ch 2

        This example shows how to enter parameters for performance calculations
        with inputs in both power and dB
        '''
        Radar(Pt=10**6, power_dB=False,
              Gt=6309.6, Gr=6309.6, g_dB=False,
              TxWfm=TxWaveform(f=3e8/0.0375, tauP=0.4e-6),
              sigmaT=3.98, tgt_sig_dB=False,
              R=60e3,
              L=2.51, l_dB=False,
              Ts=3423).SNR

        Radar(Pt=60, power_dB=True,
              Gt=38, Gr=38, g_dB=True,
              TxWfm=TxWaveform(f=3e8/0.0375, tauP=0.4e-6),
              sigmaT=6, tgt_sig_dB=True,
              R=60e3,
              L=4, l_dB=True,
              Ts=3423).SNR

    elif choice == '2':

        '''
        Example Radar Range Equation
        Based on Homework 4 from EE619
        '''

        from antenna import Antenna
        from waveform import TxWaveform
        from detection import SW0_SW5

        # Define Antenna
        Ant = Antenna(f=10e9, dim=1.5, el0=1)

        # Define Waveforms to be analyzed
        Wfm1 = TxWaveform(f=10e9, tauP=10e-6, T=1/2000, name='10us Unmod')
        Wfm2 = TxWaveform(f=10e9, tauP=25e-6, T=1/1500, name='25us Unmod')
        Wfm3 = TxWaveform(f=10e9, tauP=100e-6, T=1/1000, name='100us Unmod')
        Wfm = (Wfm1, Wfm2, Wfm3)

        # Define Target
        Tgt = SW0_SW5(6)

        # Calculate RRE and plot SNR vs Range
        radar1 = Radar(Pt=power2dB(100e3), Ant=Ant, TxWfm=Wfm, sigmaT=Tgt, Fn=4, L=14)
        radar1.plot_snr()

        # Calculate detection range for SNR requirement of 20 dB
        print('Detection Range for 20 dB target = ' +
              str(np.round(radar1.calc_Rdet(SNR_req=20)/1000, 1)) + ' km')

    elif choice == '3':

        '''
        Example Radar Range Equation
        Based on Homework 4 from EE619
        '''

        from antenna import Antenna
        from waveform import TxWaveform
        from detection import SW0_SW5

        # Define Antenna
        Ant = Antenna(f=10e9, dim=1.5, el0=1)

        # Define Waveforms to be analyzed
        Wfm1 = TxWaveform(f=10e9, tauP=10e-6, T=1/2000, name='10us Unmod')

        # Define Target
        Tgt = SW0_SW5(-10)

        # Calculate RRE and plot SNR vs Range
        radar2 = Radar(Pt=power2dB(100e3), Ant=Ant, TxWfm=Wfm1, sigmaT=Tgt, Fn=4, L=14)
        radar2.plot_snr()

        # Calculate detection range for SNR requirement of 20 dB
        print('Detection Range for -10 dB target = ' +
              str(np.round(radar2.calc_Rdet(SNR_req=20)/1000, 1)) + ' km')
