# -*- coding: utf-8 -*-
"""
Created on Thu Nov 16 23:57:50 2017

@author: Edwards
"""

__all__ = ['_constants', 'antenna', 'clutter', 'detection', 'general', 'noise',
           'performance', 'processors', 'target', 'waveform']

__version__ = 0.0.1


'''

To do list:




'''
