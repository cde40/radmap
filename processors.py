# -*- coding: utf-8 -*-
"""
Module that contains classes and functions for modeling and simulating
different types of radar processors such as MTI, pulsed Doppler, SAR, etc.
"""

import numpy as np
import scipy as sp
from scipy import special
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from _constants import pi
from numpy import prod, sqrt, exp, abs, mean, sin, tan
from general import Rdot2fd, power2dB, fd2Rdot, dB2power, tau2R, R2tau, fci
from waveform import tri


class MTI():
    '''
    Class defining Moving Target Indicator (MTI) processor

    Parameters
    ----------
    TxWfm : list
        list of waveforms used in MTI
    n : int
        order of MTI
    Rdot_max : float (default 1000 m/s)
        maximum range rate (m/s)

    Methods
    -------
    plot_resp
        plots MTI response vs frequency or speed


    '''

    def __init__(self, TxWfm, n, Rdot_max=1000):

        # Add waveform to instance waveform library
        if isinstance(TxWfm, tuple):
            self.TxWfm = TxWfm
        else:
            self.TxWfm = (TxWfm, )

        f = self.TxWfm[0].f
        for iWfm in (self.TxWfm):
            if f != iWfm.f:
                raise ValueError('All MTI waveforms must be on same RF')

        self.TxWfm = TxWfm
        self.nMTI = n
        self.nPulse = len(self.TxWfm)

        # Compute transmit time for pulses
        self.T = [0]
        for iWfm in self.TxWfm[:-1]:
            self.T.append(self.T[-1]+iWfm.T)

        # compute binomial coefficients
        b = []
        for l in list(range(self.nMTI)):
            b.append(sp.special.binom(self.nMTI-1, l))

        # Compute MTI gain
        self.K = sqrt(prod(range(2, 2*(self.nMTI), 2)) /
                      (2**(2*(self.nMTI-1)) *
                      prod(range(1, 2*(self.nMTI)-1, 2))))

        # Compute MTI output
        self.f = np.linspace(0, Rdot2fd(Rdot_max, self.TxWfm[0].f), 501)
        vSP = np.zeros((self.f.size, self.nPulse-1), dtype=complex)
        for k in list(range(self.nMTI-1, self.nPulse)):
            for l in list(range(self.nMTI)):
                vSP[:, k-1] = vSP[:, k-1] + self.K*(-1)**l*b[l]*exp(1j*2*pi*self.f*self.T[k-l])
        self.vSP = vSP
        self.H = mean(abs(vSP[:, self.nMTI:self.nPulse+1])**2, axis=1)

    def plot_resp(self, newFig=True, xaxis='Rdot', dB=True, ymin=-30):
        '''
        Plot normalized MTI response

        Parameters
        ----------
        newFig : bool (default True)
            if true, creates plot in new figure
        xaxis : {'Rdot' (default), 'freq'}
            specifies what x axis is, either range rate or frequency
        dB : bool (default True)
            if True, plot in dB
        ymin : float
            minimum y value to be plotted

        '''

        if xaxis == 'Rdot':
            x = fd2Rdot(self.f, self.TxWfm[0].f)
            xlabel = 'Range Rate (m/s)'
        else:
            x = self.f/1e3
            xlabel = 'Frequency (kHz)'

        H = self.H
        if dB:
            H = power2dB(H)
            ylabel = 'MTI Gain (dB)'
        else:
            ymin = 0
            ylabel = 'MTI Gain'

        if newFig:
            plt.figure()

        plt.plot(x, H)
        plt.grid('on')
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.title('MTI Velocity Response')
        plt.ylim(ymin=ymin)

class Doppler():
    '''
    Class defining Doppler processor

    Parameters
    ----------
    dRmin : float


    '''

    def __init__(self, Radar, dRmin=-30):
        self.dRmin = dRmin
        self.phi = dB2power(Radar.phi)
        self.TxWfm = Radar.TxWfm

        self.maxPRF = 0
        for iWfm in Radar.TxWfm:
            self.maxPRF = max((self.maxPRF, iWfm.PRF))
        self.f = np.linspace(-self.maxPRF/2, self.maxPRF/2, 5000).reshape((-1, 1))



        n = 10

        self.HH = []
        self.HB = []
#        self.Gn = []
#        self.Gs = []
#        self.GSNR = []
#        self.Gc = []
#        self.Gphi = []
#        self.GCNR = []
#        self.ISCR = []
        for iWfm in Radar.TxWfm:

            # Define Butterworth HPF
            fch = -2*self.dRmin/iWfm.lam//500*500
            beta = tan(pi*self.f*iWfm.T)
            betach = tan(pi*fch*iWfm.T)
            self.HH.append(((beta/betach)**(2*n))/(1+(beta/betach)**(2*n)))

            # Define Bandpass Integrator (FFT)
            fb = Rdot2fd(Radar.Target.dR, iWfm.f)
            self.HB.append(abs((sin(pi*iWfm.ppd*(self.f-fb)*iWfm.T) /
                           (pi*iWfm.ppd*(self.f-fb)*iWfm.T)) /
                            (sin(pi*(self.f-fb)*iWfm.T)/
                           (pi*(self.f-fb)*iWfm.T)))**2)

            BW = np.ptp(self.f[(abs(self.f) < self.maxPRF/2) & (self.HB[-1] > 0.5)])

#            self.Gn.append(BW*iWfm.T)
#            self.Gs.append(1)
#            self.GSNR.append(1/self.Gn[-1])
#            self.Gc.append(0)
#            self.Gphi.append(self.Gn[-1]*self.phi/iWfm.tauP)
#            self.GCNR.append((self.Gc[-1]+self.Gphi[-1])/self.Gn[-1])
#            self.ISCR.append(self.Gs[-1]/(self.Gc[-1]+self.Gphi[-1]))

    def plot_resp(self, newFig=True):

        if newFig:
            plt.figure()

        for i in list(range(len(self.Gs))):
            plt.plot(self.f/1e3, power2dB(self.HH[i]),
                     label='HPF : TxWfm ' + str(i))
            plt.plot(self.f/1e3, power2dB(self.HB[i]),
                     label='BPF : TxWfm ' + str(i))
        plt.grid()
        plt.xlabel('Freq (kHz)')
        plt.ylabel('Filter Response (dB)')
        plt.ylim(ymin=-60, ymax=10)
        plt.legend()

    def get_Gn(self):
        Gn = []
        for i in list(range(len(self.TxWfm))):
            BW = np.ptp(self.f[(abs(self.f) < self.maxPRF/2) &
                               (self.HB[i] > 0.5)])
            Gn.append(BW*self.TxWfm[i].T)
        return Gn

    def get_Gs(self):
        return (1, )*len(self.TxWfm)

    def get_GSNR(self):
        return list(1/np.array(self.get_Gn()))

    def get_Gc(self):
        return (0, )*len(self.TxWfm)

    def get_Gphi(self):
        Gn = np.array(self.get_Gn())
        Gphi = []
        for i in list(range(len(self.TxWfm))):
            Gphi.append(Gn[i]*self.phi/self.TxWfm[i].tauP)
        return Gphi

    def get_GCNR(self):
        Gc = np.array(self.get_Gc())
        Gphi = np.array(self.get_Gphi())
        Gn = np.array(self.get_Gn())
        return list((Gc+Gphi)/Gn)

    def get_ISCR(self):
        Gc = np.array(self.get_Gc())
        Gphi = np.array(self.get_Gphi())
        Gs = np.array(self.get_Gs())
        return list(Gs/(Gc+Gphi))

#class SAR():
#
#    def __init__():
#
#    def set_imaged_area(self, x0=20000, y0=200, w=50, l=50):
#        self.w = w
#        self.l = l
#        self.x0 = x0
#        self.y0 = y0
#        self.r0 = np.sqrt(x0**2 + y0**2)
#        self.theta_squint = np.arcsin(y0/self.r0)
#
#    def set_platform(self, V=50, L=600):
#        # selfform related paramters
#        self.V = V
#        self.L = L
#        self.T_L = L/V
#
#    def set_tx_waveform(self, BW=0):
#        # Waveform related paramters
#        self.tau_P = (3 + 1/3)*1e-9
#        if BW > 0:
#            self.tau_u = 50e-6
#            self.mod = 'LFM'
#        else:
#            self.mod = 'Unmod'
#        self.dx = self.tau_P/(2*c)
#        self.dy = 1
#        self.lambd = 0.03
#        self.T = 50e-3
#
#    def set_scatterers(self, xi=0, yi=0, Psi=1):
#        self.xi = np.reshape(xi, (-1, 1, 1))
#        self.yi = np.reshape(yi, (-1, 1, 1))
#        self.Psi = np.reshape(Psi, (-1, 1, 1))
#
#    def simulate_rx_signal(self):
#        # Calculate timing paramters
#        tau_min = 2*(self.x0 - self.l/2)/c
#        tau_max = 2*np.sqrt((self.x0 + self.l/2)**2 +
#                            (np.abs(self.y0) + self.w/2 + self.L/2)**2)/c
#
#        # Calculate number of downrange and crossrange samples
#        self.M = np.ceil((tau_max-tau_min)/self.tau_P)
#        self.K = self.T_L/self.T+1
#        self.k, self.m = np.meshgrid(np.arange(self.K) - (self.K-1)/2,
#                                     np.arange(self.M))
#
#        # Calculate range cells positions, m
#        self.x = self.m[:, 0]*self.dx
#
#        # Gross Doppler
#        self.f_dg = 2*self.V/self.lambd*self.y0/self.r0
#
#        # Time vector for each sample, k
#        t = self.k*self.T + tau_min + self.m*self.tau_P
#
#        # Define received signal
#        ri = np.sqrt((self.x0+self.xi)**2 + (self.y0+self.yi-self.V*t)**2)
#        tau_ri = tau_min + self.m*self.tau_P - 2*ri/c
#        if self.mod == 'Unmod':
#            mf_resp = wf.tri(tau_ri, self.tau_P)
#        elif sellf.mod == 'LFM':
#            mf_resp = wf.sinc(tau_ri/self.tau_P)*wf.rect(tau_ri, self.tau_u)
#        self.v = np.sum(np.sqrt(self.Psi)*np.exp(-1j*2*np.pi*self.f_dg*t) *
#                        np.exp(-1j*4*np.pi*ri/self.lambd)*mf_resp, 0)
#
#    def rance_cell_migration_correction(self):
#        # Take FFT across range bins for each sample, k
#        nfft = 2**np.ceil(np.log2(self.M)).astype(np.int)
#        V = np.fft.fft(self.v, nfft, 0)
#
#        # Apply delta_tau phase shift at each sample, k, and IFFT
#        delta_tau = 2*(np.sqrt(self.x0**2 +
#                               (self.y0 - self.V*self.k[0, :]*self.T)**2) -
#                       self.x0)/c
#        f = np.fft.fftfreq(nfft, self.tau_P).reshape((-1, 1))
#        self.v = np.fft.ifft(np.exp(1j*2*np.pi*f*delta_tau)*V, nfft, 0)
#
#    def quadratic_phase_correction(self):
#        self.v = np.exp(1j*2*np.pi*(self.V**2/(self.lambd*self.r0)) *
#                                (self.k*self.T)**2)
#
#    def image_refinement(self):
#        # Correct linear phase (Doppler Frequency) offset
#        self.v = np.exp(1j*2*np.pi*(2*self.y0*self.V/(self.lambd*self.r0)) *
#                        (self.m*self.dx/self.r0)*(self.k*self.T)) * \
#                 self.v
#
#        # Correct quadratic phase offset
#        self.v = np.exp(-1j*np.pi*(2*self.V**2/(self.lambd*self.r0)) *
#                        (self.m*self.dx/self.r0)*(self.k*self.T)**2) * \
#                 self.v
#
#    def process_sar_signal(self, os_factor=1):
#        # Take FFT of each range cell across each sample, k
#        nfft = 2**np.ceil(np.log2(self.K*os_factor)).astype(np.int)
#        print(nfft)
#        self.V = np.fft.fftshift(np.fft.fft(self.v, nfft, 1), 1)
#        f = np.fft.fftshift(np.fft.fftfreq(nfft, self.T))
#        self.y = self.lambd*f*self.r0/(2*self.V)
#
#    def trim_image(self):
#        self.V = V[np.abs(self.x <= self.l/2), np.abs(self.y <= self.w/2)]
#        self.x = self.x[np.abs(self.x <= self.l/2)]
#        self.y = self.y[np.abs(self.y <= self.w/2)]
#
#    def view_sar_image(self):
#        plt.imshow(np.abs(self.V))


class StepFM():
    def __init__(self, TxWfm, N, df):
        self.TxWfm = TxWfm
        self.N = N
        self.df = df

        self.dR = tau2R(1/(N*df), 1)
        self.Ramb = tau2R(1/df, 1)

    def plot_resp(self, tau0=None, f0=None, newFig=True, plot3D=False):

        tau = np.linspace(-1*R2tau(self.Ramb), 1*R2tau(self.Ramb), 1023).reshape((-1, 1))
        f = np.linspace(-2/self.TxWfm.T, 2/self.TxWfm.T, 1023).reshape((-1, 1))
        if tau0 is not None:
            tau = tau0
        if f0 is not None:
            f = f0
        TAU, F = np.meshgrid(tau, f)
        X = abs(sin(self.N*pi*(TAU*self.df-F*self.TxWfm.T)) / \
                np.ma.masked_array(sin(pi*(TAU*self.df-F*self.TxWfm.T)),
                                   sin(pi*(TAU*self.df-F*self.TxWfm.T)) == 0)) * \
                tri(TAU, self.TxWfm.tauP)

        X = np.squeeze(X/np.max(np.max(X)))

        if newFig:
                plt.figure()

        if tau0 is not None and f0 is None:
            plt.plot(f.reshape((-1, 1))/1e3, X)
            plt.xlabel('Doppler (kHz)')
            plt.ylabel('|$\chi$(' + str(tau0*1e6) + ' $\mu$s, f)|')
            if tau0 == 0:
                plt.title('Matched Range Doppler Cut')
            else:
                plt.title('Doppler Cut for Range Delay $\\tau$ = ' +
                          str(tau0*1e6) + ' $\mu$s')
            plt.grid()
        elif tau0 is None and f0 is not None:
            plt.plot(tau.reshape((-1, 1))*1e6, X)
            plt.xlabel('Range Delay $\\tau$ ($\mu$s)')
            plt.ylabel('|$\chi(\\tau$, ' + str(f0/1e6) + ' MHz)|')
            if f0 == 0:
                plt.title('Matched Doppler Range Cut')
            else:
                plt.title('Range Cut for Doppler f = ' + str(f0/1e6) + ' MHz')
            plt.grid()
        elif tau0 is None and f0 is None and not plot3D:
            plt.pcolormesh(TAU*1e6, F/1e6, X.T, vmin=0, vmax=1,
                           cmap='viridis', edgecolor='none')
            plt.xlabel('Range Delay $\\tau$ ($\mu$s)')
            plt.ylabel('Doppler (MHz)')
            plt.title('Ambiguity Function')
            plt.xlim(xmin=-self.TxWfm.tauP*1e6, xmax=self.TxWfm.tauP*1e6)
        elif tau0 is None and f0 is None and plot3D:
            ax = plt.gca(projection='3d')
            ax.plot_surface(TAU*1e6, F/1e6, X.T, vmin=0, vmax=1,
                            cmap='viridis', edgecolor='none',
                            rcount=128, ccount=128)
            plt.xlabel('Range Delay $\\tau$ ($\mu$s)')
            plt.ylabel('Doppler (MHz)')
            plt.title('Ambiguity Function')
            plt.xlim(xmin=-self.TxWfm.tauP*1e6, xmax=self.TxWfm.tauP*1e6)


if __name__ == '__main__':

    print('\nExamples for Processors module')
    print('-----------------------------')
    print('1. MTI Example 1')
    print('2. Stepped LFM Example')
    print('0. Exit')
    choice = input('Select example : ')

    if choice == '1':

        '''
        MTI Example
        '''

        from waveform import TxWaveform

        # 600 and 540 usec PRI repeated 5 times
        Wfm = (TxWaveform(f=5e9, tauP=10e-6, T=600e-6),
               TxWaveform(f=5e9, tauP=10e-6, T=540e-6))*5

        mti1 = MTI(Wfm, n=3)
        mti1.plot_resp()

    elif choice == '2':

        '''
        Stepped LFM Example
        '''

        from waveform import TxWaveform

        # 600 and 540 usec PRI repeated 5 times
        Wfm = TxWaveform(f=5e9, tauP=2e-6, T=600e-6)

        slfm = StepFM(Wfm, N=5, df=1e6)
        slfm.plot_resp(f0=0)
