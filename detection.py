# -*- coding: utf-8 -*-
"""
Module for conducing RCS analysis

Contains functions for generating functions for all Swerling models, plotting
their PDF and CDF, generating random values matching specified distribution and
simulating RCS history over time.

"""

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from general import dB2power
from _constants import c
from numpy import exp, sqrt, min
from scipy.misc import factorial


class RCS():
    """
    Superclass for RCS objects

    Parameters
    ----------
    none

    Returns
    -------
    none

    """

    def __init__(self, sigma_av=0, dB=True):
        if dB:
            self.sigma = 10**(sigma_av/10)
        else:
            self.sigma = sigma_av

        self.model = self.__class__.__name__

    def cdf(self, sigma_max=20, sigma_res=0.01):
        """
        Return cummulative distribution function (CDF)

        Parameters
        ----------
        sigma_max : float (default 20 dBsm)
            maximum sigma to calculate CDF for (in dBsm)
        sigma_res : float (default 0.01 dBsm)
            resolution to calculate sigma over
        newFig : Boolean (default True)
            create plot in a new figure

        Returns
        -------
        array
            CDF for RCS object corresponding to sigma

        """
#        sigma = np.linspace(0, sigma_max+sigma_res, int(sigma_max//sigma_res))
        pdf = self.pdf(sigma_max=sigma_max, sigma_res=sigma_res)
        return np.cumsum(pdf)/np.sum(pdf)

    def rvs(self, N, sigma_max=20, sigma_res=0.01):
        """
        Return random variates from distribution

        Parameters
        ----------
        N : int
            number of samples from distribution
        sigma_max : float (default 20 dBsm)
            maximum sigma to calculate CDF for (in dBsm)
        sigma_res : float (default 0.01 dBsm)
            resolution to calculate sigma over

        Returns
        -------
        array
            RCS values from given Swerling model corresponding to sigma

        """
        sigma = np.linspace(0, sigma_max+sigma_res, int(sigma_max//sigma_res))
        rand_y = np.random.uniform(0, 1, N)
        cdf = self.cdf(sigma_max=sigma_max, sigma_res=sigma_res)
        return np.interp(rand_y, cdf, sigma)

    def plot_pdf(self, sigma_max=20, sigma_res=0.01, newFig=True):
        """
        Plot probability distribution function (PDF)

        Parameters
        ----------
        sigma_max : float (default 20 dBsm)
            maximum sigma to calculate CDF for (in dBsm)
        sigma_res : float (default 0.01 dBsm)
            resolution to calculate sigma over
        newFig : Boolean (default True)
            create plot in a new figure

        Returns
        -------
        none

        """
        if newFig:
            plt.figure()

        sigma = np.linspace(0, sigma_max+sigma_res, int(sigma_max//sigma_res))
        plt.plot(sigma, self.pdf(sigma_max=sigma_max, sigma_res=sigma_res))
        plt.grid('on')
        plt.title('Probability Density Function of Target')
        plt.xlabel('$\sigma$ (m$^2$)')
        plt.ylabel('F($\sigma$)')

    def plot_cdf(self, sigma_max=20, sigma_res=0.01, newFig=True):
        """
        Plot cummulative distribution function (CDF)

        Parameters
        ----------
        sigma_max : float (default 20 dBsm)
            maximum sigma to calculate CDF for (in dBsm)
        sigma_res : float (default 0.01 dBsm)
            resolution to calculate sigma over
        newFig : Boolean (default True)
            create plot in a new figure

        Returns
        -------
        none

        """
        if newFig:
            plt.figure()

        sigma = np.linspace(0, sigma_max+sigma_res, int(sigma_max//sigma_res))
        plt.plot(sigma, self.cdf(sigma_max, sigma_res))
        plt.grid('on')
        plt.title('Cumulative Density Function of Target')
        plt.xlabel('$\sigma$ (m$^2$)')
        plt.ylabel('F($\sigma$)')


class SW0_SW5(RCS):
    """
    Swerling 0 / Swerling 5 (constant RCS) target model
    """

    def pdf(self, sigma_max=20, sigma_res=0.01):
        """
        Return probability distribution function (PDF)

        Parameters
        ----------
        sigma_max : float (default 20 dBsm)
            maximum sigma to calculate CDF for (in dBsm)
        sigma_res : float (default 0.01 dBsm)
            resolution to calculate sigma over

        Returns
        -------
        array
            PDF values corresponding to sigma

        """
        sigma = np.linspace(0, sigma_max+sigma_res, int(sigma_max//sigma_res))
        return abs(sigma-self.sigma) == min(abs(sigma-self.sigma))

    def pd(self, SNR, dB=True, Pfa=10**-6, det_type='S', n=None):
        """
        Return single pulse probability of detection

        Parameters
        ----------
        SNR : array
            signal to noise ratio
        Pfa : float
            probability of false alarm
        det_type : {'S' (default), 'N', 'C', 'M'}
            {single pulse, noncoherent integration, coherent integration, mofn}

        Returns
        -------
        array
            PDF values corresponding to sigma

        """
        if dB:
            SNR = dB2power(SNR)

        if det_type.upper() == 'S':
            TNR = -np.log(Pfa)
            return marcumq(sqrt(2*SNR), sqrt(2*TNR))

        elif det_type.upper() == 'N':
            TNR = sp.special.gammaincinv(n, 1-Pfa)
            temp = 0
            for r in list(range(2, n+1)):
                temp += exp(-TNR-n*SNR)*(TNR/(n*SNR))**((r-1)/2) * \
                        sp.special.iv(r-1, 2*np.sqrt(TNR*n*SNR))
            return marcumq(sqrt(2*n*SNR), sqrt(2*TNR)) + temp
        elif det_type.upper() == 'C':
            TNR = -np.log(Pfa)
            return marcumq(sqrt(2*n*SNR), sqrt(2*TNR))
        elif det_type.upper() == 'M':
            if len(n) != 2:
                raise ValueError('For m of n detection, n must be tuple')

            Pmofn = 0
            m, n = n
            Pd = marcumq(sqrt(2*SNR), sqrt(-2*np.log(Pfa)))
            for k in list(range(m, n+1)):
                Pmofn = factorial(n) / \
                        (factorial(k)*factorial(n-k)) * \
                        Pd**(k)*(1-Pd)**(n-k)
            return Pmofn


class SW1(RCS):
    """
    Swerling 1 (many equi-sized scatters) target model that changes slowly on
    dwell-to-dwell basis

    """

    def pdf(self, sigma=None, sigma_max=20, sigma_res=0.01):
        """
        Return probability distribution function (PDF)

        Parameters
        ----------
        sigma_max : float (default 20 dBsm)
            maximum sigma to calculate CDF for (in dBsm)
        sigma_res : float (default 0.01 dBsm)
            resolution to calculate sigma over

        Returns
        -------
        array
            PDF values corresponding to sigma

        """
        if sigma is None:
            sigma = np.linspace(0, sigma_max+sigma_res, int(sigma_max//sigma_res))

        return 1/self.sigma*exp(-sigma/self.sigma)

    def pd(self, SNR, dB=True, Pfa=10**-6, det_type='S', n=None):
        """
        Return single pulse probability of detection

        Parameters
        ----------
        SNR : array
            signal to noise ratio
        Pfa : float
            probability of false alarm
        det_type : {'S' (default), 'N', 'C', 'M'}
            {single pulse, noncoherent integration, coherent integration, mofn}

        Returns
        -------
        array
            probability of detect values for given SNR and Pfa

        """
        if dB:
            SNR = dB2power(SNR)

        if det_type.upper() == 'S':
            return exp(np.log(Pfa)/(SNR+1))
        elif det_type.upper() == 'N':
            TNR = sp.special.gammaincinv(n, 1-Pfa)
            return 1-sp.special.gammainc(n-1, TNR) + \
                (1+1/(n*SNR))**(n-1) * \
                sp.special.gammainc(n-1, TNR/(1+1/(n*SNR))) * \
                exp(-TNR/(1+n*SNR))
        elif det_type.upper() == 'C':
            return exp(np.log(Pfa)/(n*SNR+1))
        elif det_type.upper() == 'M':
            if len(n) != 2:
                raise ValueError('For m of n detection, n must be tuple')

            Pmofn = 0
            m, n = n
            x = np.linspace(0, 30, 500)
            x = x[1:]
            TNR = -np.log(Pfa)
            Q1 = marcumq(sqrt(2*x), sqrt(2*TNR))
            for k in list(range(m, n+1)):
                Pmofn += factorial(n) / (factorial(k)*factorial(n-k)) * \
                        np.trapz(Q1**k*(1-Q1)**(n-k) *
                                 exp(-x/SNR)/SNR, x=x, dx=x[1]-x[0])
            raise ValueError('M-of-N detection for SW1 not currently working.')
            return Pmofn

    def sim(self, f=5e9, tmax=30, dt=0.1, seed=None, out='RCS'):
        '''
        Simulate RCS return for Swerling 1 target for specified time

        Parameters
        ----------
        f : float (default 5 GHz)
            frequency (Hz)
        tmax : float
            time to run simulation over (s)
        dt : float
            time step in simulation (s)
        seed : int (default : None)
            seed for random number generator
        out : string (default 'RCS')
            type of output, RCS (magnitude), phase, or both
        '''

        # Generate Chi-squared distribution
        if seed is not None:
            np.random.seed(0)

        x1 = np.random.randn(int((tmax+dt)/dt), 1)
        x2 = np.random.randn(int((tmax+dt)/dt), 1)
        x = x1 + 1j*x2

        # Filter with brick wall filter (FFT-based)
        nfft = np.max((int(2**np.ceil(np.log2(x1.size))), 256))
        X = np.fft.fft(np.real(x), n=nfft, axis=0)
        freq = np.fft.fftfreq(X.size)

        # Filter
        # Based on pg 68 of Budge where used 0.5 Hz filter for 8 GHz and
        # 5 Hz filter for 90 GHz
        BW = np.max(((5-0.5)/(90e9-8e9)*f+0.061, 0.5))
        X[abs(freq) >= BW*dt] = 0

        # Return output
        y = np.fft.ifft(X, n=nfft, axis=0)
        y = sqrt(self.sigma/np.var(y))*y

        RCS = np.abs(0.5*y[0:x1.size]**2)
        Phase = np.angle(y[0:x1.size])
        if out.upper() == 'RCS':
            return RCS
        elif out.upper() == 'PHASE':
            return Phase
        elif out.upper() == 'BOTH':
            return (RCS, Phase)


class SW2(SW1):
    """
    Swerling 2 (many equi-sized scatters) target model that changes rapidly
    on a pulse-to-pulse basis
    """
    def pd(self, SNR, dB=True, Pfa=10**-6, det_type='S', n=None):
        """
        Return single pulse probability of detection

        Parameters
        ----------
        SNR : array
            signal to noise ratio
        Pfa : float
            probability of false alarm
        det_type : {'S' (default), 'N', 'C', 'M'}
            {single pulse, noncoherent integration, coherent integration, mofn}

        Returns
        -------
        array
            probability of detect values for given SNR and Pfa

        """
        if dB:
            SNR = dB2power(SNR)

        if det_type.upper() in ('S', 'C'):
            return exp(np.log(Pfa)/(SNR+1))
        elif det_type.upper() == 'N':
            TNR = sp.special.gammaincinv(n, 1-Pfa)
            return 1-sp.special.gammainc(n, TNR/(1+SNR))
        elif det_type.upper() == 'M':
            if len(n) != 2:
                raise ValueError('For m of n detection, n must be tuple')

            Pmofn = 0
            m, n = n
            Pd = exp(np.log(Pfa)/(SNR+1))
            for k in list(range(m, n+1)):
                Pmofn = factorial(n) / \
                        (factorial(k)*factorial(n-k)) * \
                        Pd**(k)*(1-Pd)**(n-k)
            return Pmofn

    def sim(self, f=5e9, tmax=30, dt=0.1, seed=None, out='RCS'):
        '''
        Simulate RCS return for Swerling 2 target for specified time

        Parameters
        ----------
        f : float (default 5 GHz)
            frequency (Hz)
        tmax : float
            time to run simulation over (s)
        dt : float
            time step in simulation (s)
        seed : int (default : None)
            seed for random number generator
        out : string (default 'RCS')
            type of output, RCS (magnitude), phase, or both
        '''
        # Generate Chi-squared distribution
        if seed is not None:
            np.random.seed(0)

        x1 = np.random.randn(int(tmax/dt)+1, 1)
        x2 = np.random.randn(int(tmax/dt)+1, 1)
        x = x1 + 1j*x2

        # Return output
        y = sqrt(self.sigma/np.var(x))*x

        RCS = np.abs(0.5*y[0:x1.size]**2)
        Phase = np.angle(y[0:x1.size])

        if out.upper() == 'RCS':
            return RCS
        elif out.upper() == 'PHASE':
            return Phase
        elif out.upper() == 'BOTH':
            return (RCS, Phase)


class SW3(RCS):
    """
    Swerling 3 (main scatter with smaller scatters) target model that changes
    slowly on dwell-to-dwell basis
    """

    def pdf(self, sigma=None, sigma_max=20, sigma_res=0.01):
        """
        Return probability distribution function (PDF)

        Parameters
        ----------
        sigma_max : float (default 20 dBsm)
            maximum sigma to calculate CDF for (in dBsm)
        sigma_res : float (default 0.01 dBsm)
            resolution to calculate sigma over

        Returns
        -------
        array
            PDF values corresponding to sigma

        """
        if sigma is None:
            sigma = np.linspace(0, sigma_max+sigma_res, int(sigma_max//sigma_res))

        return 4*sigma/self.sigma**2*exp(-2*sigma/self.sigma)

    def pd(self, SNR, dB=True, Pfa=10**-6, det_type='S', n=None):
        """
        Return single pulse probability of detection

        Parameters
        ----------
        SNR : array
            signal to noise ratio
        Pfa : float
            probability of false alarm
        det_type : {'S' (default), 'N', 'C', 'M'}
            {single pulse, noncoherent integration, coherent integration, mofn}

        Returns
        -------
        array
            probability of detect values for given SNR and Pfa

        """
        if dB:
            SNR = dB2power(SNR)

        if det_type.upper() == 'S':
            return (1-(2*SNR*np.log(Pfa))/(2+SNR)**2) * \
                    exp(2*np.log(Pfa)/(2+SNR))
        elif det_type.upper() == 'N':
            TNR = sp.special.gammaincinv(n, 1-Pfa)
            return (1+2/(n*SNR))**(n-2) * \
                (1+TNR/(1+n*SNR/2)-(2*(n-2))/(n*SNR)) * \
                exp(-TNR/(1+n*SNR/2))
        elif det_type.upper() == 'C':
            return (1-(2*n*SNR*np.log(Pfa))/(2+n*SNR)**2) * \
                    exp(2*np.log(Pfa)/(2+n*SNR))
        elif det_type.upper() == 'M':
            if len(n) != 2:
                raise ValueError('For m of n detection, n must be tuple')

            Pmofn = 0
            m, n = n
            x = np.linspace(0, 30, 500)
            x = x[1:]
            TNR = -np.log(Pfa)
            Q1 = marcumq(sqrt(2*x), sqrt(2*TNR))
            for k in list(range(m, n+1)):
                Pmofn += factorial(n) / (factorial(k)*factorial(n-k)) * \
                        np.trapz(Q1**k*(1-Q1)**(n-k) *
                                 (2*x/SNR)**2*exp(-2*x/SNR), x=x, dx=x[1]-x[0])
            raise ValueError('M-of-N detection for SW1 not currently working.')
            return Pmofn

    def sim(self, f=5e9, tmax=30, dt=0.1, seed=None, out='RCS'):
        '''
        Simulate RCS return for Swerling 3 target for specified time

        Parameters
        ----------
        f : float
            frequency (Hz)
        tmax : float
            time to run simulation over (s)
        dt : float
            time step in simulation (s)
        seed : int (default : None)
            seed for random number generator
        out : string (default 'RCS')
            type of output, RCS (magnitude), phase, or both
        '''
        # Generate Chi-squared distribution
        if seed is not None:
            np.random.seed(0)
        x1 = np.random.randn(int(tmax/dt)+1, 1)
        x2 = np.random.randn(int(tmax/dt)+1, 1)
        xa = x1 + 1j*x2
        x3 = np.random.randn(int(tmax/dt)+1, 1)
        x4 = np.random.randn(int(tmax/dt)+1, 1)
        xb = x3 + 1j*x4

        # Filter with brick wall filter (FFT-based)
        nfft = np.max((int(2**np.ceil(np.log2(x1.size))), 256))
        Xa = np.fft.fft(np.real(xa), n=nfft, axis=0)
        Xb = np.fft.fft(np.real(xb), n=nfft, axis=0)
        freq = np.fft.fftfreq(Xa.size)

        # Filter
        # Based on pg 68 of Budge where used 0.5 Hz filter for 8 GHz and
        # 5 Hz filter for 90 GHz
        BW = np.max(((5-0.5)/(90e9-8e9)*f+0.061, 0.5))
        Xa[abs(freq) >= BW*dt] = 0
        Xb[abs(freq) >= BW*dt] = 0

        # Return output
        ya = np.fft.ifft(Xa, n=nfft, axis=0)
        yb = np.fft.ifft(Xb, n=nfft, axis=0)

        ya = sqrt(self.sigma/np.var(ya))*ya
        yb = sqrt(self.sigma/np.var(yb))*yb

        RCS = 0.5*(np.abs(0.5*ya[0:x1.size]**2)+np.abs(0.5*yb[0:x1.size]**2))
        Phase = 0.5*(np.angle(ya[0:x1.size]) + np.angle(yb[0:x1.size]))
        if out.upper() == 'RCS':
            return RCS
        elif out.upper() == 'PHASE':
            return Phase
        elif out.upper() == 'BOTH':
            return (RCS, Phase)


class SW4(SW3):
    """
    Swerling 4 (main scatter with smaller scatters) target model and changes
    rapidly on a pulse-to-pulse basis
    """
    def pd(self, SNR, dB=True, Pfa=10**-6, det_type='S', n=None):
        """
        Return single pulse probability of detection

        Parameters
        ----------
        SNR : array
            signal to noise ratio
        Pfa : float
            probability of false alarm
        det_type : {'S' (default), 'N', 'C', 'M'}
            {single pulse, noncoherent integration, coherent integration, mofn}

        Returns
        -------
        array
            probability of detect values for given SNR and Pfa

        """
        if dB:
            SNR = dB2power(SNR)

        if det_type.upper() in ('S', 'C'):
            return (1-(2*SNR*np.log(Pfa))/(2+SNR)**2) * \
                    exp(2*np.log(Pfa)/(2+SNR))
        elif det_type.upper() == 'N':
            TNR = sp.special.gammaincinv(n, 1-Pfa)

            temp = 0
            for k in list(range(0, n+1)):
                temp += factorial(n)/(factorial(k) *
                                              factorial(n-k)) * \
                    (SNR/2)**-k * sp.special.gammainc(2*n-k, 2*TNR/(SNR+2))

            return 1-(SNR/(SNR+2))**n * temp
        elif det_type.upper() == 'M':
            if len(n) != 2:
                raise ValueError('For m of n detection, n must be tuple')

            Pmofn = 0
            m, n = n
            Pd = (1-(2*SNR*np.log(Pfa))/(2+SNR)**2) * \
                    exp(2*np.log(Pfa)/(2+SNR))
            for k in list(range(m, n+1)):
                Pmofn = factorial(n) / \
                        (factorial(k)*factorial(n-k)) * \
                        Pd**(k)*(1-Pd)**(n-k)
            return Pmofn

    def sim(self, f=5e9, tmax=30, dt=0.1, seed=None, out='RCS'):
        '''
        Simulate RCS return for Swerling 4 target for specified time

        Parameters
        ----------
        f : float (default 5 GHz)
            frequency (Hz)
        tmax : float
            time to run simulation over (s)
        dt : float
            time step in simulation (s)
        seed : int (default : None)
            seed for random number generator
        out : string (default 'RCS')
            type of output, RCS (magnitude), phase, or both
        '''
        # Generate Chi-squared distribution
        if seed is not None:
            np.random.seed(0)
        x1 = np.random.randn(int(tmax/dt)+1, 1)
        x2 = np.random.randn(int(tmax/dt)+1, 1)
        xa = x1 + 1j*x2
        x3 = np.random.randn(int(tmax/dt)+1, 1)
        x4 = np.random.randn(int(tmax/dt)+1, 1)
        xb = x3 + 1j*x4

        # Return output
        ya = sqrt(self.sigma/np.var(xa))*xa
        yb = sqrt(self.sigma/np.var(xb))*xb

        RCS = 0.5*(np.abs(0.5*ya[0:x1.size]**2)+np.abs(0.5*yb[0:x1.size]**2))
        Phase = 0.5*(np.angle(ya[0:x1.size]) + np.angle(yb[0:x1.size]))
        if out.upper() == 'RCS':
            return RCS
        elif out.upper() == 'PHASE':
            return Phase
        elif out.upper() == 'BOTH':
            return (RCS, Phase)


def uncorr_rtn(L):
    '''
    change in frequency to uncorrelate target returns (turn SW1/3 to SW2/4)
    '''
    return c/(2*L)


#def marcumq(a, b):
#    """
#    Marcum Q function of order 1
#
#    Parameters
#    ----------
#    a : float
#        SNR
#    b : float
#        TNR
#
#    Returns
#    -------
#    float
#        probability of detect
#
#    """
#    return sp.stats.ncx2.cdf(x=a, df=2, nc=b)


def marcumq(a, b):
    '''
    Based on Steen Parl algorithm described in Budge pg 247

    Parameters
    ----------
    a : float
        SNR
    b : float
        TNR

    Returns
    -------
    float
        probability of detect

    '''
    inshape = a.shape
    if isinstance(a, float) | isinstance(a, int):
        a = [a]

    Q = []

    # termination parameter (typically between 3 and 9)
    p = 9

    # Loop over each a
    for a in a:
        # Initialization
        beta = [0, 0.5]

        if a < b:
            alpha = [0, 1]
            d = [0, 0, a/b]
        else:
            alpha = [0, 0]
            d = [0, 0, b/a]

        # Iterate
        n = 1
        while beta[-1] <= 10**p:
            alpha.append(d[-1] + 2*n/(a*b)*alpha[-1] + alpha[-2])
            beta.append(1 + 2*n/(a*b)*beta[-1] + beta[-2])
            d.append(d[-1]*d[2])
            n += 1

        if a < b:
            Q.append(alpha[-1]/(2*beta[-1])*np.exp(-(a-b)**2/2))
        else:
            Q.append(1-alpha[-1]/(2*beta[-1])*np.exp(-(a-b)**2/2))

    return np.array(Q).reshape(inshape)


if __name__ == '__main__':

    print('\nExamples for Detection module')
    print('-----------------------------')
    print('1. SW0 / SW5 Target PDF and CDF')
    print('2. SW1 / SW2 Target PDF and CDF')
    print('3. SW3 / SW4 Target PDF and CDF')
    print('4. SW1 / SW2 RCS Simulation')
    print('5. SW3 / SW4 RCS Simulation')
    print('0. Exit')
    choice = input('Select example : ')

    if choice == '1':

        '''
        -----------------------------------------------------------------------
        Example SW0/SW5 Target
        '''
        SW0_tgt = SW0_SW5(5)
        print('\nSW0 / SW5 Target Model Example')
        print(SW0_tgt.__dict__)
        print('')

        # Plot PDF and CDF
        SW0_tgt.plot_pdf(newFig=True)
        SW0_tgt.plot_cdf(newFig=False)
        plt.legend(['PDF', 'CDF'])
        plt.gca().set_title('SW0 Target PDF and CDF')

    elif choice == '2':
        '''
        -----------------------------------------------------------------------
        Example SW1/SW2 Target
        '''
        SW1_tgt = SW1(5)
        print('\nSW1 / SW2 Target Model Example')
        print(SW1_tgt.__dict__)
        print('')

        # Grab 5000 random samples from target distribution
        SW1_rvs = SW1_tgt.rvs(5000)

        # Plot PDF and CDF
        plt.figure()
        plt.hist(SW1_rvs, bins=np.linspace(0, 20, 100), normed=True)
        SW1_tgt.plot_pdf(newFig=False)
        SW1_tgt.plot_cdf(newFig=False)
        plt.legend(['PDF', 'CDF', 'Random Samples'])
        plt.title('SW1 Target PDF and CDF')

    elif choice == '3':

        '''
        -----------------------------------------------------------------------
        Example SW3/SW4 Target
        '''
        SW3_tgt = SW3(5)
        print('\nSW3 / SW4 Target Model Example')
        print(SW3_tgt.__dict__)
        print('')

        # Grab 5000 random samples from target distribution
        SW3_rvs = SW3_tgt.rvs(5000)

        # Plot PDF and CDF
        plt.figure()
        plt.hist(SW3_rvs, bins=np.linspace(0, 20, 100), normed=True)
        SW3_tgt.plot_pdf(newFig=False)
        SW3_tgt.plot_cdf(newFig=False)
        plt.legend(['PDF', 'CDF', 'Random Samples'])
        plt.title('SW3 Target PDF and CDF')

    elif choice == '4':

        '''
        -----------------------------------------------------------------------
        Example SW1/SW2 Target RCS Simulation
        '''
        SW1_tgt = SW1(5)
        SW2_tgt = SW2(5)
        t = np.arange(0, 20+0.1, 0.1)
        plt.figure()
        plt.plot(t, 20*np.log10(SW1_tgt.sim(tmax=20, dt=0.1)))
        plt.plot(t, 20*np.log10(SW2_tgt.sim(tmax=20, dt=0.1)))
        plt.title('SW1 and SW2 Target Simulation')
        plt.xlabel('Time (sec)')
        plt.ylabel('RCS (dB)')
        plt.grid()
        plt.xlim((0, 20))
        plt.ylim((-30, 30))
        plt.legend(('SW1', 'SW2'))

    elif choice == '5':
        '''
        -----------------------------------------------------------------------
        Example SW3/SW4 Target RCS Simulation
        '''
        SW3_tgt = SW3(5)
        SW4_tgt = SW4(5)
        t = np.arange(0, 20+0.1, 0.1)
        plt.figure()
        plt.plot(t, 20*np.log10(SW3_tgt.sim(tmax=20, dt=0.1)))
        plt.plot(t, 20*np.log10(SW4_tgt.sim(tmax=20, dt=0.1)))
        plt.title('SW3 and SW4 Target Simulation')
        plt.xlabel('Time (sec)')
        plt.ylabel('RCS (dB)')
        plt.grid()
        plt.xlim((0, 20))
        plt.ylim((-30, 30))
        plt.legend(('SW3', 'SW4'))
