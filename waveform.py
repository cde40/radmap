# -*- coding: utf-8 -*--
"""
Created on Sun Nov 19 22:13:21 2017

@author: Edwards
"""

import numpy as np
import numpy.matlib
import matplotlib.pyplot as plt
import pandas as pd

from general import fd2Rdot, fci, power2dB
from _constants import c, pi, nan
from target import Target
from mpl_toolkits.mplot3d import Axes3D
from numpy import diff, exp, angle, abs, round, arange, linspace, max, sinc, sqrt
from numpy.fft import fft, ifft, fftshift, fftfreq


class Waveform():
    '''
    Super class for Waveform objects

    Methods
    ----------
    gen
    plot
    ambig

    '''

    def calc_fs(self):
        if self.mod.upper() == 'UNMOD':
            Fs = 1/self.tauP
        elif self.mod.upper() == 'LFM':
            Fs = self.BW
        elif self.mod.upper() == 'PSK' and self.BW > 0:
            Fs = self.BW
        # if tauC is not specified need to distribute phases over tauP
        elif self.mod.upper() == 'PSK' and self.tauC is None and self.BW == 0:
            Fs = 1/self.tauC

        return 10*Fs

    def gen(self, Fs=None):
        '''
        Generate baseband signal (f=0) based on instance parameters
        '''
        # Determine sample frequency and time vector
        if Fs is None:
            Fs = self.calc_fs()

        t = arange(0, self.tauP*1.2+1/Fs, 1/Fs)

        # Define unmod
        if self.mod.upper() == 'UNMOD':
            return rect(t-self.tauP/2, self.tauP)+0j, t
        elif self.mod.upper() == 'LFM':
            return exp(1j*pi*self.BW/self.tauP*t**2) * rect(t-self.tauP/2, self.tauP), t
        elif self.mod.upper() == 'PSK':
            # need to upsample phase
            phase = np.zeros(t.shape)

            # determine phase transition times
            ptt = round(self.tauC*Fs*arange(0., self.phase.size)).astype(int)
            for i in range(1, len(ptt)):
                phase[ptt[i-1]:ptt[i]] = self.phase[i]

            return exp(1j*pi*phase)*rect(t-self.tauP/2, self.tauP), t

    def plot_spec(self, Fs=None, nfft=None, newFig=True):
        '''
        Plot frequency spectrum of signal

        Parameters
        ----------
        newFig : bool (default True)
            if True, creates plot in new figure

        '''
        if Fs is None:
            Fs = self.calc_fs()
        y, t = self.gen(Fs=Fs)

        if nfft is None:
            N = max((int(2**np.ceil(np.log2(y.size))), 128))
        else:
            N = nfft
        Y = fftshift(fft(y, N))

        if newFig:
            plt.figure()

        plt.plot(fftshift(fftfreq(N, d=1/Fs)/1e6), power2dB(np.abs(Y)))
        plt.grid()
        plt.xlabel('Frequency (MHz)')
        plt.ylabel('Power (dB)')

    def plot_mod(self, Fs=None, newFig=True):
        '''
        Plot AM, PM and FM of signal

        Parameters
        ----------
        newFig : bool (default True)
            if True, creates plot in new figure

        '''

        if Fs is None:
            Fs = self.calc_fs()
        y, t = self.gen(Fs=Fs)

        AM = abs(y)
        PM = angle(y)
        FM = 1/(2*pi)*np.append(0, diff(np.unwrap(PM))*Fs)
        if newFig:
            plt.figure()

        plt.subplot(3, 1, 1)
        plt.plot(t*1e6, AM)
        plt.ylabel('Amplitude')
        plt.title('AM, PM, and FM of ' + self.name)
        plt.grid()

        plt.subplot(3, 1, 2)
        plt.plot(t*1e6, FM/1e6)
        plt.ylabel('Freq (MHz)')
        plt.grid()
        if abs(self.BW) > 0:
            plt.ylim((-self.BW/1e6, self.BW/1e6))
        else:
            plt.ylim((-1/self.tauP/1e6, 1/self.tauP/1e6))

        plt.subplot(3, 1, 3)
        plt.plot(t*1e6, PM*180/pi)
        plt.xlabel('Time (us)')
        plt.ylabel('Phase (deg)')
        plt.grid()

    def calc_mf_resp(self, tau0=None, f0=None, Fs=None, Ps=1, tauR=0, psiRn=0, return_tau_f=False, norm=False):
        '''
        Parameters
        ----------
        tau0 : float (default None)
            if specified, will return ambiguity cut at tau0 range delay
            otherwise, all range delays between -tauP and tauP will be returned
        f0 : float (default None)
            if specified, will return ambiguity cut at f0 freq offset
            otherwise, all freq offsets will be returned
        '''

        if self.mod.upper() in ['UNMOD', 'LFM']:
            # Calculate ambiguity function analytically
            tau = tau0 if tau0 is not None else linspace(-self.tauP, self.tauP, 1023)
            f = f0 if f0 is not None else linspace(-1/self.tauC, 1/self.tauC, 1023)
            f = f if not np.all(np.isnan(f)) else 0
            TAU, F = np.meshgrid(tau, f)

            alpha = self.BW/self.tauP
            if np.isnan(psiRn):
                psiRn = 0

            X = sqrt(Ps) * \
                exp(1j*psiRn) * \
                exp(1j*pi*F*(tauR+TAU)) * \
                (self.tauP-abs(TAU-tauR)) * \
                abs(sinc((F-alpha*(TAU-tauR))*(self.tauP-abs(TAU-tauR)))) * \
                rect(TAU-tauR, 2*self.tauP)

        elif self.mod.upper() == 'PSK':
            Fs = Fs if Fs is not None else self.calc_fs()
            y, t = self.gen(Fs=Fs)

            # Calculate ambiguity function numerically
            # Set up frequency offset vector
            if f0 is None:
                f = linspace(-5/self.tauP, 5/self.tauP, 1023).reshape((1, -1))
                t = t.reshape((-1, 1))

                # Set waveforms
                u = y.reshape((-1, 1))
                v = u

                # Compute ambiguity function
                N = int(2**np.ceil(np.log2(u.size + v.size)))
                Fv = np.matlib.repmat(fft(v, N, axis=0), 1, f.size)
                u = np.matlib.repmat(u.reshape((-1, 1)), 1, f.size)
                Fu = fft(u*exp(1j*2*pi*np.matmul(t, f)), N, axis=0)
                X = fftshift(ifft(np.conj(Fu)*Fv, N, axis=0), axes=0)
                X = sqrt(Ps) * \
                    exp(1j*psiRn) * \
                    exp(1j*pi*F*(tauR+TAU))


                # Get time vector for X
                tau = linspace(-N/2, N/2, N)*(t[1]-t[0])

                # Crop function
                TAU, F = np.meshgrid(tau[abs(tau) <= self.tauP], f)
                X = X[np.ix_(abs(tau) <= self.tauP, abs(f[0, :]) < self.BW)]
                tau = tau[abs(tau) <= self.tauP]

                if tau0 is not None:
                    X = X[fci(tau, tau0),:]

                X = np.squeeze(X)
                tau = tau.squeeze()
                f = f.squeeze()

            else:
                X = np.correlate(y, np.exp(1j*2*pi*f0*t)*y, 'full').T

                tau = np.sort(np.append(-t[1:], t))
                f = f0

                if tau0 is not None:
                    X = X[fci(tau, tau0)]

        if norm:
            X = X * 1/self.tauP

        if return_tau_f:
            return X.T, tau, f
        else:
            return X.T

    def plot_ambig(self, tau0=None, f0=None, Fs=None, plot3D=True, newFig=True):
        '''
        Plot ambiguity functionof signal

        Parameters
        ----------
        tau0 : float (default None)
            if specified, will return ambiguity cut at tau0 range delay
            otherwise, all range delays between -tauP and tauP will be returned
        f0 : float (default None)
            if specified, will return ambiguity cut at f0 freq offset
            otherwise, all freq offsets will be returned
        plot3D : bool (default True)
            if True, ambiguity function will be plotted in 3D, else in 2D
        newFig : bool (default True)
            if True, creates plot in new figure

        '''

        # tau0 = tau0 if isinstance(tau0, np.ndarray) else np.array(tau0)
        # f0 = f0 if isinstance(f0, np.ndarray) else np.array(f0)

        X, tau, f = self.calc_mf_resp(tau0=tau0, f0=f0, return_tau_f=True)

        # Plot results
        if newFig:
            plt.figure()

        if tau0 is not None and f0 is None:
            plt.plot(f.reshape((-1, 1))/1e6, np.abs(X))
            plt.xlabel('Doppler (MHz)')
            plt.ylabel('|$\chi$(' + str(tau0*1e6) + ' $\mu$s, f)|')
            if tau0 == 0:
                plt.title('Matched Range Doppler Cut')
            else:
                plt.title('Doppler Cut for Range Delay $\\tau$ = ' +
                          str(tau0*1e6) + ' $\mu$s')
            plt.grid()
        elif tau0 is None and f0 is not None:
            plt.plot(tau.reshape((-1, 1))*1e6, np.abs(X))
            plt.xlabel('Range Delay $\\tau$ ($\mu$s)')
            plt.ylabel('|$\chi(\\tau$, ' + str(f0/1e6) + ' MHz)|')
            if f0 == 0:
                plt.title('Matched Doppler Range Cut')
            else:
                plt.title('Range Cut for Doppler f = ' + str(f0/1e6) + ' MHz')
            plt.grid()
        elif tau0 is None and f0 is None and not plot3D:
            plt.pcolormesh(tau*1e6, f/1e6, np.abs(X.T), vmin=0, vmax=1,
                           cmap='viridis', edgecolor='none')
            plt.xlabel('Range Delay $\\tau$ ($\mu$s)')
            plt.ylabel('Doppler (MHz)')
            plt.title('Ambiguity Function')
            plt.xlim(left=-self.tauP*1e6, right=self.tauP*1e6)
        elif tau0 is None and f0 is None and plot3D:
            TAU, F = np.meshgrid(tau, f)
            ax = plt.gca(projection='3d')
            ax.plot_surface(TAU*1e6, F/1e6, np.abs(X.T), vmin=0, vmax=1,
                            cmap='viridis', edgecolor='none',
                            rcount=128, ccount=128)
            plt.xlabel('Range Delay $\\tau$ ($\mu$s)')
            plt.ylabel('Doppler (MHz)')
            plt.title('Ambiguity Function')
            plt.xlim(left=-self.tauP*1e6, right=self.tauP*1e6)


class TxWaveform(Waveform):
    '''
    Defines transmit waveform
    '''
    def __init__(self, f=None,
                 tauP=None,
                 T=None, PRF=None,
                 BW=0,
                 phase=0, tauC=None,
                 ppd=None, CPI=None,
                 mod='UNMOD',
                 Fs=0,
                 name='Wfm'):

        # Assign default values
        self.name = name
        self.mod = mod
        self.f = f if f is not None else nan
        self.lam = c/self.f
        self.tauP = tauP if tauP is not None else tauP

        # Get pulse interval info
        if T is not None and PRF is None:
            self.T = T
            self.PRF = 1/T
        elif T is None and PRF is not None:
            self.PRF = PRF
            self.T = 1/PRF
        elif T is None and PRF is None:
            self.T = self.PRF = nan
        else:
            if T is not 1/PRF:
                raise ValueError('T must be 1/PRF')
            self.T = T
            self.PRF = PRF

        # Get info related to pulses per burst
        if ppd is None and CPI is None:
            self.ppd = 1
            self.CPI = self.T
        elif ppd is not None and CPI is None:
            self.ppd = int(ppd)
            self.CPI = self.ppd*self.T
        elif ppd is None and CPI is not None:
            self.CPI = CPI
            self.ppd = int(self.CPI/self.T)
        else:
            self.CPI = CPI
            self.ppd = ppd
            if self.ppd*self.T != self.CPI:
                raise ValueError('Num pulses per dwell and CPI inconsistent')

        # Get modulation related parameters
        if mod.upper() == 'UNMOD':
            if BW != 0:
                raise ValueError('BW specified, but pulse is UNMOD')
            self.BW = 0
            self.tauC = self.tauP
        elif mod.upper() == 'LFM':
            self.BW = BW
            self.tauC = 1/BW
        elif mod.upper() == 'PSK' and tauC is None and BW > 0:
            self.BW = BW
            self.tauC = 1/BW
        # if tauC is not specified need to distribute phases over tauP
        elif mod.upper() == 'PSK' and tauC is None and BW == 0:
            self.tauC = self.tauP/np.array(phase).size
            self.BW = 1/self.tauC
        self.phase = np.array(phase)

        if mod.upper() == 'PSK':
            if abs(self.tauC*self.phase.size-self.tauP) > self.tauC/2:
                raise ValueError('Ensure  PW, chip rate, and # phases are consistent')

        # Calculate range-related parameters
        # reference: Basic Radar Analysis (Budge), pg 5, eq 1.8
        self.Ramb = self.T*c/2
        # reference: Basic Radar Analysis (Budge), pg 7, eq 1.9
        self.Rmin = self.tauP*c/2
        # reference: Basic Radar Analysis (Budge), pg 8, eq 1.10
        self.Rmax = (self.T-self.tauP)*c/2
        if self.BW == 0 or np.isnan(self.BW):
            self.dR = self.tauP*c/2
        else:
            self.dR = abs(1/self.BW)*c/2
        self.dfd = self.PRF/self.ppd
        self.dRdot = fd2Rdot(self.dfd, self.f)

        # Calculate number of range and Doppler cells for detection
        if not np.isnan(self.T) and not np.isnan(self.dR):
            self.nRngCells = round((self.Rmax-self.Rmin)/self.dR)
        else:
            self.nRngCells = nan
        self.nDopCells = 1 if self.ppd == 1 else self.ppd

        if name == 'Wfm':
            name = str(self.tauP/1e6) + 'us PW'
            if self.T != nan:
                name = ' / ' + str(self.T/1e6) + 'us PRI'

class RxWaveform(Waveform):
    '''
    Defines received waveform based on transmitted waveform and target state
    '''
    def __init__(self, txwf, R=0, Rdot=0, SNR=1, tgt=None):
        if isinstance(tgt, dict):
            R = tgt['R']
            Rdot = tgt['R']
            SNR = tgt['SNR']
        elif isinstance(tgt, Target):
            if len(tgt.R) > 1:
                raise ValueError('Can only calculate RxWavform for single target state, but multiple specified. Use tgt.get_state(n) to select single condition.')
            else:
                R = tgt.R
                Rdot = tgt.Rdot
                SNR = tgt.SNR

        # Round trip time delay
        self.tauR = 2*R/c
        self.ambig_in_R = True if 2*R/c > txwf.T else False

        # Doppler return
        self.fd = np.mod(-txwf.f*(2*Rdot/c), txwf.PRF/2)
        self.fd = -txwf.f*(2*Rdot/c)
        self.ambig_in_fd = True if abs(-txwf.f*(2*Rdot/c)) > txwf.PRF/2 else False

        # phase shift due to range delay
        self.phiR = -2*pi*txwf.f*(2*R/c)

        # SNR
        self.SNR = SNR


class WfmLib():
    def __init__(self, Wfms):

        # Add waveform to instance waveform library
        if isinstance(Wfms, tuple):
            self.Wfms = Wfms
        else:
            self.Wfms = (Wfms, )

    def output(self):
        fields = ('name', 'f', 'tauP', 'T')

        dF = pd.DataFrame()
        for iWfm in self.Wfms:
            dF.append(pd.DataFrame().from_dict(iWfm.__dict__))

        print(dF)


def rect(t, tau=1):
    '''
    rect function

    reference : Basic Radar Analysis (Budge), pg 10, eq 1.16
    '''
    return abs(abs(t) <= tau/2)


def tri(t, tau=1):
    return (1-abs(t/tau))*(abs(t) < tau)


def frank_polyphase(L):
    F_L = arange(L).reshape((-1, 1))*arange(L).reshape((1, -1))
    del_phi = 2*pi/L
    return del_phi*F_L.reshape((-1, 1))


def barker(n):
    if n == 2:
        return [0, 0]
    elif n == 3:
        return [0, 0, 1]
    elif n == 4:
        return [0, 0, 0, 1]
    elif n == 5:
        return [0, 0, 0, 1, 0]
    elif n == 7:
        return [0, 0, 0, 1, 1, 0, 1]
    elif n == 11:
        return [0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1]
    elif n == 13:
        return [0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0]

def min_peak_sidelobe(n):
    if n == 15:
        return [0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1]
    elif n == 16:
        return [0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1]
    elif n == 17:
        return [0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1]
    elif n == 18:
        return [0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1]
    elif n == 19:
        return [1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1]
    elif n == 20:
        return [0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1]
    elif n == 21:
        return [1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1]
    elif n == 22:
        return [0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1]
    elif n == 23:
        return [0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1]
    elif n == 24:
        return [0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1]
    elif n == 25:
        return [1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1]


def lfsr(polydef, ifill, codelen=None, shift='right', persp='rx'):
    polydef = polydef.split('(')
    order = int(polydef[0][1:])
    taps = polydef[1][0:-1].split(',')
    if taps[0] == '0':
        taps = taps[1:]
    for i in range(len(taps)):
        taps[i] = int(taps[i])

    reg = np.zeros(order)
    for i in range(order):
        if ifill[i] == '1':
            reg[i] = 1

    if codelen is None:
        codelen = 2**order-1
    out = np.zeros(codelen)
    for i in range(codelen):
        if shift == 'right':
            out[i] = reg[-1]
            fdbk = 0
            for j in range(len(taps)):
                fdbk += reg[taps[j]-1]
            reg[1:] = reg[0:-1]
            reg[0] = fdbk % 2

        elif shift == 'left':
            out[i] = reg[0]
            fdbk = 0
            for j in range(len(taps)):
                fdbk += reg[order-(taps[j]-1)-1]
            reg[0:-1] = reg[1:]
            reg[-1] = fdbk % 2

    if persp.lower() == 'rx':
        return out
    elif persp.lower() == 'tx':
        out = np.fliplr(out.reshape((1, -1)))
        return out[0]


if __name__ == '__main__':
    print('\nExamples for Waveform module')
    print('-----------------------------')
    print('1. Transmit waveform : 10 us PW / 100 us PRI @ 10 GHz')
    print('2. Ambiguity Plot : 10 us PW / 5 MHz LFM')
    print('3. Transmit waveform : 128 pulse Doppler burst of 2 us PW / 100 kHz PRF @ 10 GHz')
    print('4. Receive waveform : From 10 km / 100 m/s target')
    print('5. Example Ambiguity Plots')
    print('0. Exit')
    choice = input('Select example : ')

    if choice == '1':

        '''
        Example Transmit Waveform
        Create a 10 usec unmodulate pulse with a 100 usec PRI
        '''
        TxWfm1 = TxWaveform(f=10e9, tauP=10e-6, T=100e-6)
        print('\nTransmit Waveform Example')
        print(TxWfm1.__dict__)

    elif choice == '2':

        '''
        Example Transmit Waveform
        Create a 10 usec unmodulate pulse with a 100 usec PRI
        '''
        TxWfm1 = TxWaveform(f=10e9, tauP=10e-6, BW=5e6, mod='LFM')
        TxWfm1.plot_ambig(plot3D=False)

    elif choice == '3':

        '''
        Example Transmit Waveform
        Create a 2 usec unmodulate pulse with a 10 usec PRI
        '''
        TxWfm1 = TxWaveform(f=10e9, tauP=2e-6, PRF=10e3, ppd=128)
        print('\nTransmit Waveform Example')
        print(TxWfm1.__dict__)

    elif choice == '4':

        '''
        Example Receive Waveform
        Determine Rx waveform for a target at 10 km range traveling at 100 m/s
        '''
        TxWfm1 = TxWaveform(f=10e9, tauP=10e-6, T=100e-6)
        RxWfm1 = RxWaveform(TxWfm1, R=10e3, Rdot=100)
        print('\nReceive Waveform Example')
        print(RxWfm1.__dict__)

    elif choice == '5':
        '''
        Example Ambiguity functions
        '''
        # Unmod
        # 2D plot
        TxWaveform(tauP=10e-6).plot_ambig(plot3D=False)
        # 3D plot
        TxWaveform(tauP=10e-6).plot_ambig(plot3D=True)
        # matched range, Doppler cut
        TxWaveform(tauP=10e-6).plot_ambig(tau0=0)
        # matched Doppler, range cut
        TxWaveform(tauP=10e-6).plot_ambig(f0=0)

        # LFM
        # 2D plot
        TxWaveform(tauP=10e-6, BW=1e6, mod='LFM').plot_ambig(plot3D=False)
        # 3D plot
        TxWaveform(tauP=10e-6, BW=1e6, mod='LFM').plot_ambig(plot3D=True)
        # matched range, Doppler cut
        TxWaveform(tauP=10e-6, BW=1e6, mod='LFM').plot_ambig(tau0=0)
        # matched Doppler, range cut
        TxWaveform(tauP=10e-6, BW=1e6, mod='LFM').plot_ambig(f0=0)

        # PSK
        # 2D plot
        TxWaveform(tauP=13e-6, BW=1e6, mod='PSK', phase=barker(13)).plot_ambig(plot3D=False)
        # 3D plot
        TxWaveform(tauP=13e-6, BW=1e6, mod='PSK', phase=barker(13)).plot_ambig(plot3D=True)
        # matched range, Doppler cut
        TxWaveform(tauP=13e-6, BW=1e6, mod='PSK', phase=barker(13)).plot_ambig(tau0=0)
        # matched Doppler, range cut
        TxWaveform(tauP=13e-6, BW=1e6, mod='PSK', phase=barker(13)).plot_ambig(f0=0)
