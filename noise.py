# -*- coding: utf-8 -*-
"""
Created on Mon Dec  4 20:04:46 2017

@author: Edwards
"""


from general import dB2power, power2dB
import numpy as np
from _constants import T0


class Device():
    """
    Base class for an type of device in receiver chain

    Methods
    ----------
    get_G
        returns gain
    get_Fn
        returns noise figure
    get_Te
        returns noise equivalent temperature
    """

    def get_G(self, dB=True):
        if dB:
            return power2dB(self.G)
        else:
            return self.G

    def get_Fn(self, dB=True):
        if dB:
            return power2dB(self.Fn)
        else:
            return self.Fn

    def get_Te(self, dB=True):
        return self.Te


class Attenuator(Device):
    """
    Define attenuator (passive) component for receiver chain

    Parameters
    ----------
    G : float
        gain
    Fn: float
        noise figure
    name : string
        name associated with component
    units : {'dB' (default), 'power'}
        indicates units of G and/or Fn is in dB or power

    Returns
    -------
    none

    """

    def __init__(self, G=None, Fn=None, name='Attenuator', dB=True):

        self.name = name

        if G is not None and Fn is None:
            if dB:
                self.G = dB2power(G)
                self.Fn = dB2power(-G)
            else:
                self.G = G
                self.Fn = 1/G
        elif G is None and Fn is not None:
            if dB:
                self.Fn = dB2power(Fn)
                self.G = dB2power(-Fn)
            else:
                self.Fn = Fn
                self.G = 1/Fn
        elif G is not None and Fn is not None:
            if dB:
                self.Fn = dB2power(Fn)
                self.G = dB2power(G)
            else:
                self.Fn = Fn
                self.G = G

        self.Te = T0*(self.Fn-1)

        if self.G > 1:
            raise ValueError('Attenuator cannot have gain, must be attenuation.')

        if not (G is not None and Fn is not None) and self.G != 1/self.Fn:
            raise ValueError('G and Fn must be equal and opposite.')

class Amplifier(Device):
    """
    Define amplifier (active) component for receiver chain

    Parameters
    ----------
    G : float
        gain
    Fn: float
        noise figure
    name : string
        name associated with component
    units : {'dB' (default), 'power'}
        indicates units of G and/or Fn is in dB or power

    Returns
    -------
    none

    """

    def __init__(self, G=None, Fn=None, name='Amplifier', dB=True):

        self.name = name

        if dB:
            self.G = dB2power(G)
            self.Fn = dB2power(Fn)
        else:
            self.G = G
            self.Fn = Fn

        self.Te = T0*(self.Fn-1)


class Receiver(Device):
    """
    Define receiver chain

    reference: Basic Radar Analysis (Budge), pg 90, eq 4.43, 4.44

    Parameters
    ----------
    components : list
        list of components in receiver chain

    Returns
    -------
    none

    """

    def __init__(self, cmp):

        self.cmp = cmp

        G = []
        self.G = []
        self.Fn = []
        self.Te = []
        for i in range(len(cmp)):
            if i == 0:
                G.append(float(cmp[i].G))
                self.G.append(float(cmp[i].G))
                self.Fn.append(cmp[i].Fn)
                self.Te.append(cmp[i].Te)
            else:
                G.append(float(cmp[i].G))
                self.G.append(self.G[-1]*float(cmp[i].G))
                self.Fn.append(sum(self.Fn) +
                               (cmp[i].Fn-1)/np.prod(np.array(G[:-1])))
                self.Te.append(sum(self.Te) +
                               (cmp[i].Te-1)/np.prod(np.array(G[:-1])))

if __name__ == '__main__':

    print('\nExamples for Performance module')
    print('-----------------------------')
    print('1. Receiver Noise Example 1')
    print('0. Exit')
    choice = input('Select example : ')

    if choice == '1':

        '''
        Receiver Noise Figure Example
        Based on EE619 Homework 7
        '''
        # Define components
        wvgd = Attenuator(G=-2, name='Waveguide')
        rfamp1 = Amplifier(G=20, Fn=6, name='RF Amplifier')
        rfamp2 = Amplifier(G=20, Fn=2, name='RF Amplifier')
        mixer = Attenuator(G=-3, Fn=10, name='Mixer')
        ifamp = Amplifier(100, 20, 'IF Amplifier')

        # Define receiver 1
        Rx1 = Receiver(cmp=(wvgd, rfamp1, mixer, ifamp))
        print('Noise figure through devices : ' +
              str(Rx1.get_Fn()) + ' dB')

        # Define receiver 2
        Rx2 = Receiver(cmp=(wvgd, rfamp2, mixer, ifamp))
        print('Noise figure through devices : ' +
              str(Rx2.get_Fn()) + ' dB')

        print('Equivalent noise temperature, T = ' + str(rfamp1.Te) + ' K')
        print('Equivalent noise temperature, T = ' + str(Rx1.Te) + ' K')
