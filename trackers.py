# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
from numpy import sqrt, cos, sin, exp
import matplotlib.pyplot as plt

from waveform import TxWaveform, RxWaveform
from target import Target
from antenna import Linear
from _constants import c, nan, pi
from general import Rdot2fd


class Tracker():

    def __init__(self, order, T, fc, discrim, zeta=0.707):
        self.order = order
        self.T = T
        self.fc = fc
        self.zeta = zeta if order == 1 else None
        self.gen_filter_coeffs()
        self.gen_tracker_matricies()
        self.Xp0 = np.zeros((self.order, 1))
        self.discrim = discrim
        self.t = None
        self.yp = None
        self.e = None

    def update_filter_rate(self, Tnew):
        self.T = Tnew
        self.gen_filter_coeffs()
        self.gen_tracker_matricies()

    def gen_filter_coeffs(self):

        # Generate filter coefficients
        if self.order == 1:
            self.a = exp(-2*self.zeta*2*pi*self.fc*self.T)
            self.b = 2*exp(-self.zeta*2*pi*self.fc*self.T)*cos(2*pi*self.fc*self.T*sqrt(1-self.zeta**2))-1

            self.alpha = nan
            self.beta = nan
            self.gamma = nan

        elif self.order == 2:
            coeff = np.array([4.302, -12.9834, 25.6727, -31.8607, 17.358])
            self.alpha = np.sum(coeff*(self.fc*self.T)**np.array(list(range(1,len(coeff)+1))))
            self.beta = self.alpha**2/(2-self.alpha)

            self.gamma = nan
            self.a = nan
            self.b = nan

        elif self.order == 3:
            coeff = np.array([4.4193, -14.1397, 29.6192, -38.1654, 21.3524])
            self.alpha = np.sum(coeff*(self.fc*self.T)**np.array(list(range(1, len(coeff)+1))))
            self.beta = self.alpha**2*(self.alpha-2+sqrt(self.alpha**2-16*self.alpha+16))/(4*(1-self.alpha))
            self.gamma = (self.beta*(2-self.alpha)-self.alpha**2)/self.alpha

            self.a = nan
            self.b = nan

    def gen_tracker_matricies(self):

        if self.order == 1:
            self.K = np.array([[-self.a/self.b], [self.a/(self.b*self.T)]])
            self.F = np.array([[1, self.T], [0, self.b]])
            self.H = np.array([1, 0])
        elif self.order == 2:
            self.K = np.array([[self.alpha], [self.beta/self.T]])
            self.F = np.array([[1, self.T], [0, 1]])
            self.H = np.array([1, 0])
        elif self.order == 3:
            self.K = np.array([[self.alpha], [self.beta/self.T], [2*self.gamma/self.T**2]])
            self.F = np.array([[1, self.T, self.T**2/2], [0, 1, self.T], [0, 0, 1]])
            self.H = np.array([1, 0, 0])

    def init_track(self, Xp0):
        self.Xp0 = np.zeros(self.K.shape)
        for iXp0 in enumerate(Xp0) if isinstance(Xp0, list) else enumerate([Xp0, ]):
            self.Xp0[iXp0[0]] = iXp0[1]

    def track(self, tgt):

        tgt = tgt if isinstance(tgt, list) else [tgt, ]

        # Initilize saved states of simulation
        Xs = self.Xp0
        self.yp = np.zeros((np.max([i.npoints for i in tgt])))
        self.e = np.zeros((np.max([i.npoints for i in tgt])))
        self.t = self.T*np.max([i.npoints for i in tgt])

        for n in range(np.max([i.R.size for i in tgt])):
            tgt_state = [itgt.get_state(n) for itgt in tgt]
            Xs = self.step(n, Xs, tgt_state)

    def step(self, n, Xs, tgt_state, *arg):
        # Prediction Stage
        Xp = self.F @ Xs
        self.yp[n] = self.H @ Xp

        # Update Stage
        self.e[n] = self.discrim.discrim(tgt_state, self.yp[n], arg)

        # Smoothing Stage
        return Xp + self.K*self.e[n]

    def plot_track_range(self, tgt):
        plt.figure()
        plt.subplot(3, 1, 1)
        plt.plot(tgt.t, tgt.R/1e3)
        plt.plot(tgt.t, self.yp*c/2/1e3)
        plt.ylabel('Range (km)')
        plt.grid(True)

        h1 = plt.subplot(3, 1, 2)
        plt.plot(tgt.t, tgt.R-self.yp*c/2)
        plt.grid(True)
        plt.ylabel(r'$\Delta$R (m)')

        h2 = plt.subplot(3, 1, 3)
        plt.plot(tgt.t, self.e*c/2)
        plt.grid(True)
        plt.xlabel('Time (s)')
        plt.ylabel('e (m)')

        yval = (np.max(np.abs([h1.get_ylim(), h2.get_ylim()])) // 5 + 1) * 5
        h1.set_ylim((-yval, yval))
        h2.set_ylim((-yval, yval))

    def plot_track_angle(self, tgt):
        plt.figure()
        plt.subplot(3, 1, 1)
        plt.plot(tgt.t, np.arcsin(tgt.u)*180/pi)
        plt.plot(tgt.t, np.arcsin(self.yp)*180/pi)
        plt.ylabel('u (deg)')
        plt.grid(True)

        h1 = plt.subplot(3, 1, 2)
        plt.plot(tgt.t, np.arcsin(tgt.u-self.yp)*180/pi)
        plt.grid(True)
        plt.ylabel(r'$\Delta$u (deg)')

        h2 = plt.subplot(3, 1, 3)
        plt.plot(tgt.t, np.arcsin(self.e)*180/pi)
        plt.grid(True)
        plt.xlabel('Time (s)')
        plt.ylabel('e (deg)')

        yval = (np.max(np.abs([h1.get_ylim(), h2.get_ylim()])) // 0.5 + 1) * 0.5
        h1.set_ylim((-yval, yval))
        h2.set_ylim((-yval, yval))


class RadarTracker():

    def __init__(self, range_trkr=None, angle_trkr=None, doppler_trkr=None):

        self.range_trkr = range_trkr
        self.angle_trkr = angle_trkr
        self.doppler_trkr = doppler_trkr

    def track(self, tgt):

        tgt = tgt if isinstance(tgt, list) else [tgt, ]

        # Initilize saved states of simulation
        if self.range_trkr is not None:
            Xs_rng = self.range_trkr.Xp0
            self.range_trkr.yp = np.zeros((np.max([i.npoints for i in tgt])))
            self.range_trkr.e = np.zeros((np.max([i.npoints for i in tgt])))
            self.range_trkr.t = self.range_trkr.T*np.max([i.npoints for i in tgt])
        if self.angle_trkr is not None:
            Xs_ang = self.angle_trkr.Xp0
            self.angle_trkr.yp = np.zeros((np.max([i.npoints for i in tgt])))
            self.angle_trkr.e = np.zeros((np.max([i.npoints for i in tgt])))
            self.angle_trkr.t = self.angle_trkr.T*np.max([i.npoints for i in tgt])
        if self.doppler_trkr is not None:
            Xs_dop = self.doppler_trkr.Xp0
            self.doppler_trkr.yp = np.zeros((np.max([i.npoints for i in tgt])))
            self.doppler_trkr.e = np.zeros((np.max([i.npoints for i in tgt])))
            self.doppler_trkr.t = self.doppler_trkr.T*np.max([i.npoints for i in tgt])

        for n in range(np.max([i.R.size for i in tgt])):

            tgt_state = [itgt.get_state(n) for itgt in tgt]

            # Range Track to set tautrk
            if self.range_trkr is not None:
                Xs_rng = self.range_trkr.step(n, Xs_rng, tgt_state)
                Vtrk = self.range_trkr.discrim.Vtrk
            else:
                Vtrk = 1

            if self.angle_trkr is not None:
                Xs_ang = self.angle_trkr.step(n, Xs_ang, tgt_state, Vtrk)

            if self.doppler_trkr is not None:
                Xs_dop = self.angle_trkr.step(n, Xs_ang, tgt_state, Vtrk)


class RangeDiscriminator():
    def __init__(self, txwfm, q=0.5, fm=0, noise=False):
        self.txwfm = txwfm
        self.q = q
        self.fm = 0
        self.Stau = (1-self.q)*self.txwfm.tauC
        self.Vtrk = nan
        self.noise = noise

    def discrim(self, tgt, tautrk, *arg):

        tgt = tgt if isinstance(tgt, list) else [tgt, ]
        if len(tgt) > 1:
            raise ValueError('Perfect range tracker is only valid for a single target')

        rxwfm = RxWaveform(self.txwfm, tgt=tgt[0])
        return rxwfm.tauR - tautrk

    def plot_discrim_curve(self, newFig=True):
        tgt = Target(R=np.linspace(-2*self.txwfm.dR, 2*self.txwfm.dR, int(4*txwfm.dR+1)))
        e = np.zeros(tgt.npoints)
        for n in range(tgt.npoints):
            e[n] = self.discrim(tgt.get_state(n), 0)

        if newFig:
            plt.figure()

        plt.plot(tgt.R/(c/2)*1e6, e*1e6)
        plt.xlabel(r'$\Delta$tau (us)')
        plt.ylabel('e (us)')
        plt.title('Sampling Gate Discriminator Curve(s)')
        plt.grid()
        plt.ylim(-self.txwfm.dR/(c/2)*1e6, self.txwfm.dR/(c/2)*1e6)


class SamplingGateDiscriminator(RangeDiscriminator):

    def __init__(self, txwfm, q=0.5, fm=0, noise=False):
        if q < 0 or q > 1:
            raise ValueError('q must be between 0 and 1')
        super().__init__(txwfm, q, fm, noise=noise)

    def discrim(self, tgt, tautrk, *arg):

        tgt = tgt if isinstance(tgt, list) else [tgt, ]

        VE, VL, self.Vtrk = 0, 0, 0
        for itgt in tgt:
            rxwfm = RxWaveform(self.txwfm, tgt=itgt)

            fm = self.fm[itgt['n']] if isinstance(self.fm, np.ndarray) else self.fm
            f = Rdot2fd(itgt['Rdot'], self.txwfm.f) - fm

            # Set sampling gate times
            tauE = tautrk - self.q*self.txwfm.tauC
            tauL = tautrk + self.q*self.txwfm.tauC

            VE += self.txwfm.calc_mf_resp(tau0=tauE, f0=f, Ps=itgt['SNR']/self.txwfm.tauP**2, tauR=rxwfm.tauR, psiRn=rxwfm.phiR)
            VL += self.txwfm.calc_mf_resp(tau0=tauL, f0=f, Ps=itgt['SNR']/self.txwfm.tauP**2, tauR=rxwfm.tauR, psiRn=rxwfm.phiR)
            self.Vtrk += self.txwfm.calc_mf_resp(tau0=tautrk, f0=f, Ps=itgt['SNR']/self.txwfm.tauP**2, tauR=rxwfm.tauR, psiRn=rxwfm.phiR)

        if self.noise:
            VE += (np.random.randn(1)+1j*np.random.randn(1))/sqrt(2)
            VL += (np.random.randn(1)+1j*np.random.randn(1))/sqrt(2)
            self.Vtrk += (np.random.randn(1)+1j*np.random.randn(1))/sqrt(2)

        VE, VL = abs(VE), abs(VL)

        return (VL-VE)/(VL+VE)*self.Stau


class AngleDiscriminator():
    def __init__(self, txwfm, ant, noise=False):
        self.txwfm = txwfm
        self.ant = ant
        self.SD = 1
        self.noise = noise
        self.ant = ant

    def discrim(self, tgt, strk, Vtrk):

        tgt = tgt if isinstance(tgt, list) else [tgt, ]

        Vsum, Vdelta = 0, 0

        for n, itgt in enumerate(tgt):
            rxwfm = RxWaveform(self.txwfm, tgt=itgt)
            Vtrk = Vtrk[n] if len(Vtrk) > 0 else np.sqrt(itgt['SNR'])

            iVsum, iVdelta = self.ant.calc_monopulse_response(itgt['u']-strk)
            Vdelta += iVdelta*Vtrk
            Vsum += iVsum*Vtrk

        if self.noise:
            Vdelta += (np.random.randn(1)+1j*np.random.randn(1))/sqrt(2)
            Vsum += (np.random.randn(1)+1j*np.random.randn(1))/sqrt(2)

        return self.calc_error(Vdelta, Vsum)

    def plot_discrim_curve(self, newFig=True):
        tgt = Target(u=np.linspace(-0.2, 0.2, 101))
        e = np.zeros(tgt.npoints)
        for n in range(tgt.npoints):
            e[n] = self.discrim(tgt.get_state(n), 0, noise=False)

        if newFig:
            plt.figure()

        plt.plot(np.arcsin(tgt.u)*180/pi, np.arcsin(e)*180/pi)
        plt.xlabel(r'$\Delta$s (deg)')
        plt.ylabel('e (deg)')
        plt.title('Angle Discriminator Curve(s)')
        plt.grid()
        plt.ylim(-15, 15)


class ExactDiscriminator(AngleDiscriminator):

    def __init__(self, txwfm, ant, noise=False):
        super().__init__(txwfm, ant, noise=noise)
        self.SD = 2/(pi*self.ant.N*self.ant.d2wl)

    def calc_error(self, Vdelta, Vsum):
        return np.real(Vdelta/Vsum)*self.SD


class ModifiedExactDiscriminator(AngleDiscriminator):

    def __init__(self, txwfm, ant, kdelta=0.2, noise=False):
        super().__init__(txwfm, ant, noise=noise)
        self.SD = 2/(pi*self.ant.N*self.ant.d2wl)
        self.kdelta = kdelta

    def calc_error(self, Vdelta, Vsum):
        return np.real(Vdelta*np.conj(Vsum)/(np.abs(Vsum)**2+self.kdelta*np.abs(Vdelta)**2))*self.SD


class LogDiscriminator(AngleDiscriminator):

    def __init__(self, txwfm, ant, noise=False):
        super().__init__(txwfm, ant, noise=noise)
        self.SD = 1/(pi*self.ant.N*self.ant.d2wl)

    def calc_error(self, Vdelta, Vsum):
        return (np.log(np.abs(Vsum+Vdelta))-np.log(np.abs(Vsum+Vdelta)))*self.SD




if __name__ == '__main__':

    print('\nExamples for Tracker module')
    print('-----------------------------')
    print('1. Unmod Discriminator Curves')
    print('2. LFM Discriminator Curves')
    print('3. Range Tracking Example 1')
    print('4. Range Tracking with Doppler Offset')
    print('5. Dichotomous Range Tracking')
    print('6. Angle Tracker')
    print('7. Integrated Range and Angle Tracker')
    print('0. Exit')
    choice = input('Select example : ')

    if choice == '1':

        '''
        Example Unmod Discriminator Curves
        Transmit waveform a 1 usec unmodulate pulse, with q = [0.25, 0.5, 0.75]
        '''
        txwfm = TxWaveform(tauP=1e-6, mod='UNMOD')
        discrims = [SamplingGateDiscriminator(txwfm, q=0.25), \
                    SamplingGateDiscriminator(txwfm, q=0.5), \
                    SamplingGateDiscriminator(txwfm, q=0.75)]
        for i in enumerate(discrims):
            i[1].plot_discrim_curve(newFig=True if i[0] == 0 else False)
        plt.legend(['q=0.25', 'q=0.5', 'q=0.75'])

    elif choice == '2':

        ''''
        Example LFM Discriminator Curves
        Transmit waveform a 15 usec LFM pulse, with q = [0.25, 0.5, 0.75]
        '''
        txwfm = TxWaveform(tauP=15e-6, BW=1e6, mod='LFM')
        discrims = [SamplingGateDiscriminator(txwfm, q=0.25), \
                    SamplingGateDiscriminator(txwfm, q=0.5), \
                    SamplingGateDiscriminator(txwfm, q=0.75)]
        for i in enumerate(discrims):
            i[1].plot_discrim_curve(newFig=True if i[0] == 0 else False)
            plt.legend(['q=0.25', 'q=0.5', 'q=0.75'])
        plt.grid(True)

    elif choice == '3':

        ''''
        Example LFM Discriminator Curves
        Transmit waveform a 1 usec unmodulated pulse
        '''
        # Define target and simulate trajectory
        target = Target(R0=50e3, Rdot0=-300, sigma=0, sigmaref=0, SNRref=10, Rref=50e3)
        target.sim_linear_traj(tmax=100, T=0.1)

        # Define tracker
        txwfm = TxWaveform(tauP=1e-6, mod='UNMOD')
        trkr = Tracker(2, 0.1, 1, SamplingGateDiscriminator(txwfm, q=0.5, noise=True))

        trkr.init_track((target.R[0]+txwfm.dR/3)/(c/2))
        trkr.track(target)
        trkr.plot_track_range(target)

    elif choice == '4':

        ''''
        Example LFM Discriminator Curves
        Transmit waveform a 15 usec LFM pulse, with q = [0.25, 0.5, 0.75]
        '''
        # Define tracker
        txwfm = TxWaveform(f=5e9, tauP=15e-6, BW=1e6, mod='LFM')
        trkr = Tracker(2, 0.1, 0.5, SamplingGateDiscriminator(txwfm, q=0.5, noise=True))

        # Define target and simulate trajectory
        target = Target(R0=50e3, sigma=0, sigmaref=0, SNRref=15, Rref=60e3)
        target.sim_racetrack_traj(tmax=200, T=trkr.T, r=10e3)

        trkr.init_track((target.R[0])/(c/2))
        trkr.track(target)
        trkr.plot_track_range(target)

    elif choice == '5':

        ''''
        Example LFM Discriminator Curves
        Transmit waveform a 15 usec LFM pulse, with q = [0.25, 0.5, 0.75]
        '''
        # Define tracker
        txwfm = TxWaveform(f=5e9, tauP=15e-6, BW=1e6, mod='LFM')
        trkr = Tracker(2, 0.1, 0.5, SamplingGateDiscriminator(txwfm, noise=True))

        # Define target and simulate trajectory
        target = [Target(R0=50e3, Rdot0=-300, sigma=2, sigmaref=0, SNRref=15, Rref=50e3),
                  Target(R0=51e3, Rdot0=-320, sigma=0, sigmaref=0, SNRref=15, Rref=50e3)]
        target[0].sim_linear_traj(tmax=100, T=trkr.T)
        target[1].sim_linear_traj(tmax=100, T=trkr.T)
        Ravg = (target[0].R+target[1].R)/2

        trkr.init_track([51e3/(c/2), -320/(c/2)])
        trkr.discrim.fm = Rdot2fd(target[0].Rdot, txwfm.f)
        trkr.track(target)

        plt.figure()
        plt.plot(target[0].t, (target[0].R-Ravg))
        plt.plot(target[1].t, (target[1].R-Ravg))
        plt.plot(target[0].t, trkr.yp*c/2-Ravg)
        plt.xlabel('Time (s)')
        plt.ylabel('Range (km)')
        plt.grid(True)

        # Change target 0 RCS
        target[0].sigma = 1
        target[0].sim_linear_traj(tmax=100, T=trkr.T)

        trkr.init_track([51e3/(c/2), -320/(c/2)])
        trkr.discrim.fm = Rdot2fd(target[0].Rdot, txwfm.f)
        trkr.track(target)

        plt.figure()
        plt.plot(target[0].t, (target[0].R-Ravg))
        plt.plot(target[1].t, (target[1].R-Ravg))
        plt.plot(target[0].t, trkr.yp*c/2-Ravg)
        plt.xlabel('Time (s)')
        plt.ylabel('Range (km)')
        plt.grid(True)

    elif choice == '6':

        txwfm = TxWaveform(f=5e9, tauP=15e-6, BW=1e6, mod='LFM')
        ant = Linear(d2wl=0.5, N=50, squint=0.78)
        # trkr = Tracker(2, 0.1, 0.5, ExactDiscriminator(txwfm, 50, 1/2, 1/(1.17*(50-1)), noise=True))
        trkr = Tracker(2, 0.1, 0.5, ExactDiscriminator(txwfm, ant, noise=True))

        # Define target and simulate trajectory
        target = Target(R0=50e3, sigma=0, sigmaref=0, SNRref=15, Rref=60e3)
        target.sim_racetrack_traj(tmax=200, T=trkr.T, r=10e3)

        trkr.init_track(0)
        trkr.track(target)
        trkr.plot_track_angle(target)

    elif choice == '7':
        # Define target and simulate trajectory
        target = Target(R0=50e3, sigma=0, sigmaref=0, SNRref=15, Rref=60e3)
        target.sim_racetrack_traj(tmax=200, T=0.1, r=10e3)

        txwfm = TxWaveform(f=5e9, tauP=15e-6, BW=1e6, mod='LFM')
        ant = Linear(d2wl=0.5, N=50, squint=0.78)
        range_trkr = Tracker(2, 0.1, 0.5, SamplingGateDiscriminator(txwfm, q=0.5, noise=True))
        angle_trkr = Tracker(2, 0.1, 0.5, ModifiedExactDiscriminator(txwfm, ant, noise=True))
        combined_trkr = RadarTracker(range_trkr=range_trkr, angle_trkr=angle_trkr)

        combined_trkr.range_trkr.init_track((target.R[0])/(c/2))
        combined_trkr.angle_trkr.init_track(target.u[0])

        combined_trkr.track(target)
        combined_trkr.range_trkr.plot_track_range(target)
        combined_trkr.angle_trkr.plot_track_angle(target)


















