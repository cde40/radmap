# -*- coding: utf-8 -*-
"""
Module contains code related to modeling clutter
"""

from general import dB2power, power2dB
from _constants import pi, Re
from math import sqrt
from detection import RCS

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt


class Clutter(RCS):
    '''
    Class defining clutter object

    Parameters
    ----------
    hT : float
        antenna height
    sigma0 : float
        clutter to noise ratio
    sigmaV : float
        clutter velocity spread (m/s)
    sigmaS : float
        variance due to scanning
    Tscan : float
        antenna scan period (s)

    Methods
    -------
    calc_sigmaC
        calculate clutter cross section
    plot_sigmaC
        plot clutter cross section vs range
    CA
        calculate clutter attenuation
    plot_pdf
        plot clutter frequency response

    Return
    ------
    Clutter object


    '''

    def __init__(self, Ant, TxWfm,
                 hT=0,
                 sigma0=-20, units='dB',
                 sigmaV=0.22, Tscan=0,
                 calcR=True, Rmax=100e3, Rres=100):

        # antenna parameters
        if Ant.h == 0:
            raise ValueError('Antenna height must be > 0')
        self.Ant = Ant
        self.Tscan = Tscan

        # target height
        self.hT = hT

        if isinstance(TxWfm, tuple):
            TxWfm = TxWfm
        else:
            TxWfm = (TxWfm, )
        self.TxWfm = TxWfm

        # Determine range extent
        iRmax = Rmax
        if calcR:
            Rmax = 0
            for iWfm in list(self.TxWfm):
                Rmax = max((min((iRmax, iWfm.Rmax*iWfm.ppd)), Rmax))
        self.R = np.arange(Rres, Rmax+Rres, Rres).reshape((-1, 1))

        # Define statistical parameters
        if units == 'dB':
            sigma0 = dB2power(sigma0)
        self.sigma0 = sigma0

        try:
            self.sigmaS = 0.265*(2*pi/(Ant.BW[1]*Tscan))
        except ZeroDivisionError:
            self.sigmaS = 0
        self.sigmaV = sigmaV

        self.sigmaF = []
        self.sigmaT = []
        for iWfm in TxWfm:
            self.sigmaF.append(2*self.sigmaV/iWfm.lam)
            self.sigmaT = np.sqrt(self.sigmaF[-1]**2 + self.sigmaS**2)

        self.calc_sigmaC()

    def calc_sigmaC(self, calcR=True, Rmax=100e3, Rres=100):
        '''
        Calculate clutter cross section

        '''

        # Account for earth curvature and antenna height
        Rh = sqrt(2*(4/3*Re)*self.Ant.h)

        iRmax = Rmax
        if calcR:
            Rmax = 0
            for iWfm in list(self.TxWfm):
                Rmax = max((min((iRmax, iWfm.Rmax*iWfm.ppd)), Rmax))
        self.R = np.arange(Rres, Rmax+Rres, Rres).reshape((-1, 1))

        # Approximate antenna gain for beam intersecting ground
        epsR = np.sin((self.hT - self.Ant.h)/self.R)
        eps = self.Ant.el0 + epsR
        G = self.Ant.mba(el=eps)

        scale = self.sigma0*self.R*self.TxWfm[0].dR/(1+(self.R/Rh)**4)
        self.sigmaMB = scale*(G[1]**2*self.Ant.BW[1])
        self.sigmaSL = scale*(pi*self.Ant.SL**2)
        self.sigmaC = self.sigmaMB + self.sigmaSL
        self.sigma = self.sigmaC

    def plot_sigmaC(self):
        '''
        Plot clutter cross section

        '''

        plt.plot(self.R/1e3, power2dB(self.sigmaC))
        plt.grid('on')
        plt.xlabel('Range (km)')
        plt.ylabel('$\sigma_C$ (dBsm)')
        plt.title('Ground Clutter RCS for $\sigma_0$ = ' +
                  str(round(power2dB(self.sigma0))) + ' dBsm')

    def CA(self):
        '''
        Calculate clutter attenuation (CA)

        Returns
        -------
        float
            clutter attenuation

        '''
        CA = []
        for iWfm in self.TxWfm:
            CA.append(2*(iWfm.PRF/(2*pi*self.sigmaT))**2)
        return CA

    def plot_pdf(self, f=np.linspace(-1e3, 1e3, 100),
                 calcF=True, ymin=-60, dB=True):
        '''
        Plot clutter frequency response

        '''

        if calcF:
            maxPRF = 0
            for iWfm in self.TxWfm:
                if iWfm.PRF > maxPRF:
                    maxPRF = iWfm.PRF

        # Compute PDF
        f = np.linspace(-5*maxPRF, 5*maxPRF, 1001)
        y = sp.stats.norm(scale=self.sigmaT).pdf(f)

        if dB:
            y = power2dB(y)
        else:
            ymin = 0

        plt.plot(f/1e3, y)
        plt.grid('on')
        plt.xlabel('Freq (kHz)')
        plt.ylabel('$\sigma$ (dBsm)')
        plt.title('PDF of Ground Clutter Spectrum')
        plt.ylim(ymin=ymin, ymax=30)

if __name__ == '__main__':

    print('\nExamples for Clutter module')
    print('-----------------------------')
    print('1. Clutter to Noise Ratio Calculation')
    print('2. Clutter PDF')
    print('3. Clutter Signal to Interferer Ratio')
    print('4. Clutter Example 1')
    print('5. Clutter Example 2')
    print('0. Exit')
    choice = input('Select example : ')

    if choice == '1':
        '''
        Example Clutter Problem
        Based on Homework 8 from EE725
        '''
        from antenna import Antenna
        from waveform import TxWaveform
        from general import rad2deg

        # Define Antenna
        Ant1 = Antenna(f=10e9, BW=(2, 2), h=5, SL=-30)


        # Define Waveforms to be analyzed
        Wfm1 = TxWaveform(f=10e9, tauP=1e-6, T=1/3000, name='10us Unmod')

        # Loop over steering angels and plot clutter
        # Steer to 0 deg, +1/2 beamwidth, and +1 beamwidth
        clutter = []
        for el in np.array([0, 0.5, 1])*rad2deg(Ant1.get_BW()[1]):

            # Steer beam
            Ant1.steer(el0=el)

            # Define clutter model
            clutter.append(Clutter(Ant1, Wfm1, sigma0=-20))

            # Plot clutter power
            clutter[-1].plot_sigmaC()

    elif choice == '2':

        '''
        More Clutter
        '''

        from antenna import Antenna
        from waveform import TxWaveform

        # Set up antenna and waveform to use in clutter
        ant1 = Antenna(8e9, BW=(2, 2), h=5)
        wfm1 = TxWaveform(8e9, tauP=10e-6, T=400e-6)

        # Calcualte model for non scanning
        cl1 = Clutter(ant1, wfm1, sigmaV=0.22, Tscan=0)
        cl1.plot_pdf()

        # Calculate mode for scanning
        cl2 = Clutter(ant1, wfm1, sigmaV=0.22, Tscan=2)
        cl2.plot_pdf()

    elif choice == '3':
        '''
        Clutter Example
        EE725 Homework 10 Problem 5
        '''

        from antenna import Antenna
        from waveform import TxWaveform
        from performance import Radar
        from detection import SW0_SW5

        # Set up antenna and waveform to use in clutter
        ant = Antenna(8e9, BW=(2, 2), h=5, SL=-30)
        wfm = TxWaveform(8e9, tauP=4e-6, T=400e-6, BW=1e6, mod='LFM')

        # Calcualte model for non scanning
        clutter = Clutter(ant, wfm, hT=150, sigma0=-20, sigmaV=0.22, Tscan=2)

        # Define Radar
        radar2 = Radar(Pt=power2dB(50e3), Ant=ant, TxWfm=wfm, Fn=4, L=10, Ts=1000)
        radar2.set_target(SW0_SW5(6))
        radar2.set_clutter(clutter)
        radar2.plot_snr_cnr_sir()

    elif choice == '4':

        '''
        SIR Example / Clutter Example
        EE725 Homework 13
        '''

        from antenna import Antenna
        from waveform import TxWaveform
        from performance import Radar
        from detection import SW0_SW5

        # Set up antenna and waveform to use in clutter
        ant = Antenna(8e9, BW=(2, 2), h=5, SL=-30, el0=1)
        wfm = TxWaveform(8e9, tauP=4e-6, T=50e-3, BW=1e6, mod='LFM')

        # Calcualte model for non scanning
        clutter = Clutter(ant, wfm, sigma0=-20, sigmaV=0.22, Tscan=2)

        # Define radar model
        radar3 = Radar(Pt=47, Ant=ant, TxWfm=wfm, Fn=4, L=10, Ts=1000)
        radar3.set_target(SW0_SW5(6))
        radar3.set_clutter(clutter)
        radar3.plot_snr_cnr_sir()

    elif choice == '5':

        from antenna import Antenna
        from waveform import TxWaveform
        from performance import Radar
        from detection import SW0_SW5
        from target import Target
        from processors import Doppler

        '''
        SIR Example / Clutter Example
        EE725 Homework 13
        '''
        # Set up antenna and waveform to use in clutter
        ant = Antenna(8e9, BW=(2, 2), h=3, SL=-30, el0=1)
        wfm = TxWaveform(8e9, tauP=1e-6, PRF=100e3, BW=1e6, mod='LFM', CPI=7e-3)

        # Calcualte model for non scanning
        clutter = Clutter(ant, wfm, sigma0=-10, sigmaV=0.22, Tscan=0)

        # Define radar model
        radar3 = Radar(Pt=10e3, power_dB=False, Ant=ant, TxWfm=wfm, Fn=4, L=10,
                       Ts=1500, phi=-130, Rmax=50e3, Rres=10)
        radar3.set_target(Target(dR=-150, sigma=SW0_SW5(-10)))
        radar3.set_clutter(clutter)
        radar3.plot_snr_cnr_sir()

        dopproc = Doppler(radar3, dRmin=30)
